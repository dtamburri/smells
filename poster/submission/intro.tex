%!TEX root = smells-sigconf.tex
%Code smells reflect sub-optimal areas or patterns of code that often lead to critical flaws or even failures in modern software. Much in the same way, community smells reflect sub-optimal organizational and socio-technical areas or patterns across the organizational structure around that software and are often if not always connected to circumstances such as code smells. Looking deeper into this connection, we conducted empirical software engineering over a total of 143 releases for 11 open-source communities and their connected software projects. We observed several key relations and overlaps across code and community smells that may prove critical in understanding and predicting code and community smells alike. We conclude that further investigating the relation between code and community smells may prove vital to find more effective ``patching" mechanisms for some smells (code or community), while completely avoiding the occurrence of others by acting on the organizational and socio-technical tissue around them.

Software engineering is, by nature, a ``social" activity that involves organizations, developers, and stakeholders who are responsible for leading to the definition of a software product that meets the expected requirements%\cite{conway1968committees}
. 
The social interactions among the involved actors may represent the key to success as well as a critical issue possibly causing the failure of the project~\cite{archcomm}.
%\as{The previous sentence seems to suggest that the social smells affect emergence of the technical debt \emph{by definition}, which is strange since we planned to investigate the relation between social smells and code smells.}\as{``Weigh heavily'' seems to be vague.}

Recently, the research community has investigated \emph{social debt}, \ie unforeseen project cost connected to a ``suboptimal" development community (\ie both in structure and behavior) \cite{TamburriKLV15}.
%, \eg  by identifying bad practices that negatively influence the development of software projects.
One of the recent advances in this research field is represented by the definition of \emph{community smells}~\cite{TamburriKLV15}, \ie a set of socio-technical characteristics (e.g., high formality) and patterns (\eg recurrent condescending behavior), which may lead to the emergence of social debt. Community smells are connected to code smells \cite{TamburriN15}.

In this paper we aim at deeply and empirically exploring the relation between social and technical debts, in particular on how \emph{community smells} influence the maintainability of \emph{code smells}~\cite{MohaG07,DiNucci}. 
We follow a convergence mixed-methods approach~\cite{PentaT17}, in which quantitative and qualitative research are run in parallel over the same dataset aiming at converging towards a confirmed theory.

%We start our empirical investigation by surveying 162 original developers of 117 releases belonging to 9 \textsc{Apache} and \textsc{Eclipse} systems in order elicit possible reasons making developers decide whether to remove code smells, with the purpose of understanding whether community-related issues might influence their decisions. This study is motivated by the fact that developers, even if they perceive code smells as design problems, they are not inclined to remove them by performing refactoring operations \cite{Bavota:jss2015, TufanoPBOPLP15, Silva:fse2016, Peters:2012, Chatzigeorgiou:2014}. The survey findings confirmed our initial conjecture. Specifically, over 80\% of practitioners explicitly mention that avoiding community problems (e.g., recurrent disagreements) and other social ``smells", is the reason why code smells are \emph{*not*} refactored, meaning that \emph{it is more convenient to keep a technical smell than dealing with a community smell}. As a consequence, they highlighted that not only the maintainability of code smells depends on technical factors but also by \emph{the other fish of the sea}, \ie the social debt occurring in software communities.

%On one hand, community smells represent sub-optimal patterns of organisational behavior (e.g., overly fragmented communication/collaboration) or characteristics in the community structure (e.g., overly formal communication structure) \cite{TamburriKLV15}. On the other hand, code smells \cite{Fowler:1999} are symptoms of the presence of poor design or implementation choices possibly affecting code maintainability \cite{KhomhPGA12,PalombaBPOL14}.
%We argue that community smells have a notable impact on the maintainability of code smells. 
%over the technical circumstances resulting in code smells; more in particular, we argue that community smells are often what may be causing code to smell bad \cite{TufanoPBOPLP15} in the first place. 
%\as{Isn't this too strong for our statistical analysis?}


%\as{since we are doing survey-data analysis-survey, this is neither explanatory nor exploratory. I would keep it to just mixed methods here.} 
%\as{I've rephrased the previous sentence again since we did not explicitly ask for community-related issues but they emerged...}
%\as{Original sentence:``to understand if a correlation does exist and then elicit the community-related factors that developers perceive as related to code-smell maintainability''. This is not what we do, we need to somehow be more clear about the difference between the first and the second survey. I have tried to rephrase this but this might require rephrasing RQ1 as well. What do you think?}

%\as{This 80\% finding should be explicitly reported in Section~\ref{sec:survey} as one of the main findings of the first survey.}

%On the basis of such a result, we decide to quantitatively evaluate to which extent do the community-related factors impact code smell intensity, i.e., an estimation of the severity of a code smell, and provide a novel code smell intensity prediction model that explicitly consider community-related factors when predicting the future intensity of code smells. We systematically investigate the relationship between four community smells \cite{sdebt,TamburriKLV15}, \ie \emph{Organisational Silo}, \emph{Black Cloud}, \emph{Lone Wolf}, and \emph{Bottleneck}, and five code smells \cite{Brown:1998,Fowler:1999}, \ie \emph{Long Method}, \emph{Feature Envy}, \emph{Blob}, \emph{Spaghetti Code}, and \emph{Misplaced Class} following a mixed-methods research approach~\cite{Easterbrook2008}. As a result, we found that a code smell intensity prediction model built using community smells is able to more accurately predict the future intensity of code smells than a model that does not explicitly consider the status of software communities. To qualitatively assess the latent relation between community smells and code smells we also surveyed five project managers.% that were asked to evaluate the likelihood of the appearance of the relationships between the community and code smells investigated. 

%Key results of the study show that: (a) over 80\% of surveyed practitioners themselves explicitly mention that avoiding community problems (e.g., recurrent disagreements) and other social ``smells", is the reason why code smells are \emph{*not*} addressed, meaning that it is more convenient to keep a technical smell than dealing with a community smell; (b) community smells seem to induce a ``theme" onto code smells, for example the \emph{Organizational Silo Effect} community smell, i.e., presence of two isolated developer subcommunities, strongly correlates with a code smell that reflect ``isolated" code such as the \emph{Blob}; (c) a code smell intensity prediction model built using community-related factors is able to more accurately predict the future intensity of code smells than a model that does not explicitly consider the status of software communities. 

Our main results indicate that presence of community smells hinders refactoring of the code smells.
In particular, we identify occurrence of two known and four previously unknown community smells; and show that community smells are relevant for all code smells and are often more relevant than traditional community-related measures such as socio-technical congruence.

%In summary, as original research contributions of this article we: (1) automatically detect instances of 4 community and 5 code smells over 117 releases of 10 open-source systems; (2) survey practitioners of those systems inquiring on the reason why code smells are not refactored, revealing the occurrence of 2 known and 4 previously unknown community smells; (3)  evaluate the overlaps between practitioners' perceived code smell causes and any community smells we reported; (4) prepare a prediction model able to predict the intensity of code smells using community structure information as well. 
%Further, we make the anonymized survey data, software metrics, and the rest of our data publicly available to foster replication \cite{Shull2008}.

%\as{Now I'm not sure anymore what the difference should be between (3) and (4).}
%\as{5 contributions---too much?}
%compare code smell causes resulting from the survey with the automatically detected community smells; \as{This is imprecise since the survey referred in (3) is actually our second survey while the sentence itself suggests that it is the same survey as in (2), i.e., the first survey.} (4)
%other social (\eg communicating vs. collaborating contributors ratio) and socio-technical (\eg socio-technical congruence~\cite{damiansociotechnicaltse}) factors mediate strongly in the code vs. community smell interaction we observed - for example, both technical and community smells remain stable until socio-technical congruence breaks. 

%We address the following research questions:\\
%
%:\as{I thought that we started with the survey? Please check different mixed-method strategies described by Creswell~\cite{Creswell2008} and summarized by Easterbrook et al.~\cite{Easterbrook2008}, and phrase this discussion of mixed methods accordingly.}
%\as{What is the difference between circumstances and patterns?}
%\begin{itemize}
%	\item {\bf RQ$_1$:} \emph{To what extent do community and technical smells co-occur together?}
%\smallskip
%	\item {\bf RQ$_2$:} \emph{What community and technical smells relate more with each other?}\smallskip	
%	\item {\bf RQ$_3$:} \emph{Is there a relationship between the number of community- and code-smells affecting a software system during its evolution?}\smallskip	
%	\item {\bf RQ$_4$:} \emph{Is there a relationship between the introduction of community smells and that of technical smells?}\smallskip	
%	\item {\bf RQ$_5$:} \emph{Is there a relationship between the removal of community smells and that of technical smells?}
%	
%\item 
%{\bf RQ1.} \emph{are there any community structure characteristics and recurrent problems that influence the maintainability of code smells?} --- that is, are code smells caused by sub-optimal community characteristics such as community smells and is code-smell resolution complicated by community smells themselves?\\
%\as{Is the notion of ``code smell maintainability'' defined somewhere?}
%\as{RQ1: something is wrong with the grammar.}
%\item 
%{\bf RQ2.} \emph{To what extent can community smells explain the increasing of code smells?} --- that is, are community smells a mediator for the increased recurrence of code smells? Industrial qualitative evidence in previous work \cite{TamburriKLV15} shows that they do; in this study we set out to evaluate, both qualitatively and quantitatively, whether the same is true in open-source and to what extent.\\
%\as{Sorry, restoring the comment: do we know a priori that the smells increase? If yes, please use this to motivate RQ2.}
%\smallskip
%\item 
%{\bf RQ3.} \emph{Does a community-aware code smell prediction model improve the performance of models that do not consider this information?} --- that is, does a community-aware statistical prediction model predict code smell presence better than prediction models that do not take community structure and problems into account?\\
%\as{Is this the same as ``Does a community-aware model predicts code smell intensity better than the prediction models that do not take community into account?''? Should the model be aware of the community (e.g., truck factor) or more specifically of the community smells?}
%\end{itemize}

%\cite{Fowler:1999}, \ie symptoms of the presence of poor design or implementation choices possibly affecting the maintainability of the source code \cite{KhomhPGA12,PalombaBPOL14}. \as{All those ``possiblies'' sound strange: community smell might cause code smell that might lead to maintainability problems. Do we expect some kind of stronger relation here? Fabio has mentioned studies linking code smells to maintainability more explicitly.}
%\as{Do we have statistical evidence for the mediation effects?}
%\as{This sounds a bit too specific to me: as far as I recall the practitioners did not refer to smells, they tried to avoid collaboration problems that we associate with presence of social smells}
%\as{Not sure whether ``casting a theme'' is an existing English expression}
%\as{I've tried to explain the Silo and to make the link with the ``isolation'' explicit}

%Our observations lead to conclude that understanding, predicting, and fixing code and community smells are activities that need to take both perspectives, namely, social and technical, in equal account. For example, defect prediction techniques could well benefit in precision and recall by accounting for the recurrent presence of sub-optimal social and organisational circumstances. Similarly, code refactoring tasks need to be organised according to (1) the organisational structure that refactors *and* (2) the sub-optimal patterns therein.
% \as{I do not understand the last sentence at all.}

%More research is needed to further understand and fully characterize these relations between community and code smells, for a better comprehension of how to manage and reduce both social \cite{sdebt} and technical debt \cite{tdsmell}. This work is the first of its kind to propose such exploration upon solid quantitative empirical grounds, with remotely similar examples in the domain of requirements engineering for coordination by Cataldo \etal \cite{CataldoHC08} and similar quantitative studies in closed-source by Damian \etal \cite{damiansociotechnicaltse}. Both these lines of inquiry and related research, however, are still far from relating the notion of community smells (\ie recurrent sub-optimal organizational and socio-technical circumstances and patterns) to code smells (\ie sub-optimal technical code circumstances) with the goal of comprehending their co-creation and evolution. 
%\ANDY{Should we move this sentence to the conclusion? This is personal, but I think it can have more ``impact'' there} 
%\as{Is ``more research is needed'' not too weak?}

%The rest of this paper is structured as follows. Sections \ref{sec:survey} and \ref{sec:rd} outline our research design and results for our research questions. Section \ref{sec:threats} address the threats to validity we detected and addressed. Section \ref{sec:related} outlines related work. Finally, Section \ref{sec:conc} concludes the paper.
%\footnote{Damian's work is partially supported by the European Commission grant no. 644869 (H2020 - Call 1), DICE}.
