% !TEX root = smells-ieeetran.tex
%Code smells reflect sub-optimal areas or patterns of code that often lead to critical flaws or even failures in modern software. Much in the same way, community smells reflect sub-optimal organizational and socio-technical areas or patterns across the organizational structure around that software and are often if not always connected to circumstances such as code smells. Looking deeper into this connection, we conducted empirical software engineering over a total of 143 releases for 11 open-source communities and their connected software projects. We observed several key relations and overlaps across code and community smells that may prove critical in understanding and predicting code and community smells alike. We conclude that further investigating the relation between code and community smells may prove vital to find more effective ``patching" mechanisms for some smells (code or community), while completely avoiding the occurrence of others by acting on the organizational and socio-technical tissue around them.

Software engineering is, by nature, a ``social" activity that involves organizations, developers, and stakeholders who are responsible for leading to the definition of a software product that meets the expected requirements \cite{conway1968committees}. 
\revised{The social interactions among the involved actors can represent the key to success but can also be a critical issue possibly causing additional project costs from an organizational and socio-technical perspective \cite{conway1968committees,archcomm}}.
%\as{The previous sentence seems to suggest that the social smells affect emergence of the technical debt \emph{by definition}, which is strange since we planned to investigate the relation between social smells and code smells.}\as{``Weigh heavily'' seems to be vague.}

\revised{In the recent past, the research community devoted effort to understanding so-called \emph{social debt} \cite{sdebt}, which refers to the presence of non-cohesive development communities whose members have communication or coordination issues that make them unable to tackle a certain development problem and that can lead to unforeseen project cost.}
One of the recent advances in this research field is represented by the definition of \emph{community smells}, which were defined by Tamburri \etal \cite{archcomm,TamburriKLV15} as a set of socio-technical characteristics (e.g., high formality) and patterns (\eg repeated condescending behavior, or rage-quitting), which may lead to the emergence of social debt. \minor{From a more actionable and analytical perspective, community smells are nothing more than \emph{motifs} over a graph \cite{Alon2007}; motifs are recurrent and statistically significant sub-graphs or patterns over a graph detectable using either the structural properties and fashions of the graph or the graph salient features and characteristics (e.g., colors in the case of a colored graph). For example, the \emph{organizational silo effect} \cite{TamburriKLV15} is a recurring network sub-structure featuring highly decoupled community structures.}


In turn, community smells are often connected to circumstances such as technical debt \cite{TamburriN15}, \ie the implementation of a poor implementation solution that will make the maintainability of the source code harder.
	
\revised{In this paper we aim at empirically exploring the relation between social and technical debt, by investigating the connection between two noticeable symptoms behind such types of debt: community and code smells. The latter refer to poor implementation decisions \cite{Fowler:1999} that may lead to a decrease of maintainability \cite{palomba2017diffuseness} and an increase of the overall project costs \cite{SjobergYAMD13}.}

\begin{center}
	\begin{conjecturebox}
		\revised{\emph{We conjecture that the presence of community smells can influence the persistence of code smells, as the circumstances reflected by community smells (e.g., lack of communication or coordination between team members) may lead the code to be less maintainable, making code smells worse and worse over time.}}
	\end{conjecturebox}
\end{center}

\revised{Our empirical investigation features a convergence mixed-methods approach \cite{Creswell_Research02,PentaT17,JohOnw04} (see Figure \ref{approach}) where quantitative and qualitative research are run in parallel over the same dataset with the goal of converging towards theoretical saturation. As mentioned, the theoretical saturation in question is to be achieved via mixed-methods convergence; our assumption is therefore that, if both qualitative and quantitative data lead to the same conclusions, then our theory is saturated. Conversely, any disagreement between the two theories would lead us to an improved version of our initial theory that code and community smells are somehow connected.}

Following this research method, our \textbf{qualitative} investigation features a survey of 162 original developers of 117 releases belonging to 9 \textsc{Apache} and \textsc{Eclipse} systems in order to answer the research question below: \smallskip
\begin{itemize}
	\item {\bf RQ1:} \emph{What concerns affect the developers' decision to eliminate or preserve code smells?}\smallskip
\end{itemize}
In other words, we aim at eliciting possible reasons for making developers decide whether to remove code smells; the purpose of this study is understanding whether community-related issues might influence developer decisions to retain or remove a code smell. This study is motivated by the fact that developers, even if they perceive code smells as implementation problems, are not inclined to remove them by performing refactoring operations \cite{Bavota:jss2015, Chatzigeorgiou:2014, Peters:2012, Silva:fse2016, TufanoPBOPLP15}. \revised{The survey findings confirmed our initial hypothesis, as over 80\% of practitioners explicitly mention that avoiding community problems (\eg repeated disagreements)
%, five previously known, and four previously unknown community smells, 
is the reason why code smells are \emph{not} refactored. This means that \emph{it is more convenient to keep a technical smell than deal with a community smell}. Thus, the survey findings highlighted that the persistence of code smells not only depends on technical factors studied by past literature \cite{palomba2017diffuseness,KhomhPGA12,palomba2017scent}, but also on \emph{the other fish of the sea}, \ie additional aspects related to the social debt occurring in software communities that have not been studied yet.}
	
\begin{figure}[tb]
\begin{center}
\includegraphics[width=0.9\linewidth]{exp/approach}
\end{center}
\caption{Convergence Mixed-Methods, qualitative and quantitative inquiry converge towards a confirmed theory \cite{Creswell_Research02,PentaT17,JohOnw04}.}\label{approach}
\end{figure}

In parallel with the qualitative inquiry, we \textbf{quantitatively} evaluate to what extent the community-related factors measured over the 9 projects in our dataset impact code smell intensity \cite{ARCELLIFONTANA201743,DBLP:conf/icsm/FontanaFZR15}, \ie an estimation of the severity of a code smell:\smallskip

	\begin{itemize}
	
	\item {\bf RQ2:} \emph{To what extent can community smells explain the increase of code smell intensity?}\smallskip
	
	\item {\bf RQ3:} \emph{Does a community-aware code smell intensity prediction model improve the performance of models that do not consider this information?}	\smallskip
\end{itemize}

\revised{We present a novel code smell intensity prediction model that explicitly considers community-related factors when predicting the future intensity of code smells, with the aim of providing developers and project managers with a practical technique that would allow them to preventively take actions that preserve the maintainability of the source code (\eg refactoring of the team composition). To this aim, we systematically investigate the relationship between \emph{all} the automatically detectable community smells \cite{sdebt,TamburriKLV15}, \ie \emph{Organizational Silo}, \emph{Black Cloud}, \emph{Lone Wolf}, and \emph{Bottleneck}, and five code smells, \ie \emph{Long Method}~\cite{Fowler:1999}, \emph{Feature Envy}~\cite{Fowler:1999}, \emph{Blob}~\cite{Brown:1998}, \emph{Spaghetti Code}~\cite{Brown:1998}, and \emph{Misplaced Class}. All these code  smells turned out to be relevant from the developers' perspective in our survey study.} As a result, we found that a code smell intensity prediction model built using community smells is able to more accurately predict the future intensity of code smells than a model that does not explicitly consider the status of software communities. \revised{The accuracy of the devised prediction model is also confirmed by ten industrial project managers, who were surveyed to qualitatively assess the latent relation between the community and code smells considered by our model.}

%Key results of the study show that: (a) over 80\% of surveyed practitioners themselves explicitly mention that avoiding community problems (e.g., repeated disagreements) and other social ``smells", is the reason why code smells are \emph{*not*} addressed, meaning that it is more convenient to keep a technical smell than dealing with a community smell; (b) community smells seem to induce a ``theme" onto code smells, for example the \emph{Organizational Silo Effect} community smell, i.e., presence of two isolated developer subcommunities, strongly correlates with a code smell that reflect ``isolated" code such as the \emph{Blob}; (c) a code smell intensity prediction model built using community-related factors is able to more accurately predict the future intensity of code smells than a model that does not explicitly consider the status of software communities. 

\medskip
\textbf{Contributions and Implications.} In summary, the original research contributions of this article are: 

	\begin{itemize}
	
		\item A large survey study involving 162 practitioners aimed at analysing the reasons why code smells are not refactored; 
		
		\item As a side effect of the survey study, we reveal the existence of 4 previously unknown community smells;
		
		\item A large-scale quantitative study where we assess the impact of community-related information on the performance of a code smell intensity prediction model.	
	
		\item \revised{A comprehensive replication package, containing anonymised qualitative and quantitative data used in our study \cite{appendix}.}
		
	\end{itemize}

\revised{Our study has relevant implications for researchers, practitioners, and tool vendors:}

	\begin{enumerate}
		
		\item \revised{Our findings represent a call for \emph{community-aware} software evolution techniques, that explicitly consider community-related factors to recommend practitioners how to evolve their code. Thus, both researchers and tool vendors should take into account those aspects when developing new tools;}
		
		\item \revised{Practitioners should carefully monitor the evolution of software communities, and, emergence of community smells. If needed, practitioners should take preventive actions;}
		
		\item \revised{Our study promotes a \emph{comprehensive research approach} for software maintenance and evolution: indeed, we show that the circumstances occurring within the development community directly affect the way developers act in the source code. Thus, our study encourages researchers to take into account community-related aspects when studying the underlying dynamics of software evolution.}
		
	\end{enumerate}
		
\medskip
\revised{\textbf{Structure of the paper.} Section \ref{sec:terminology} introduces the terminology we use in the paper, while Sections \ref{sec:survey} and \ref{sec:rd} outline our research design and results for our research questions. Section \ref{converge} outlines a theoretical convergence between the two sides of our study, while \ref{sec:threats} address the threats to validity we detected and addressed. Section \ref{sec:related} outlines related work. Finally, Section \ref{sec:conc} concludes the paper.}

	
%{\color{blue}To clarify, the community smells detected in the quantitative part were already previously known in the state of the art. With the qualitative study we aimed at: (1) confirming the presence of community smells as a phenomenon at large; (2) connecting the presence of community smells with cause-effect relations over code smells. At the same time, with the quantitative part we aimed at: (1) revealing the co-occurrence of code and (previously known) community smells; (2) looking at the effect size of detection power by including community smells in said detection power. We succeeded in achieving theoretical saturation for all of the points above by: (a) using Grounded-Theory, on a qualitative sense; (b) using a quantitative approach and statistical modelling, on a quantitative sense.}

%{\color{blue}Both the studies converge towards one single conclusion: community smells are repeated and statistically relevant community-related factors perceived for the pervasiveness and permanence of code smells.} They are considered as key mediators of the persistence of code smells, as is confirmed by most developers in our qualitative survey or by our quantitative statistical model, as major factors of the persistence of code smells. This conclusion supports the need for joint measurement and management mechanisms to control technical \cite{debtissue,tdebt} and social debt \cite{archcomm,TamburriKLV15} at the same time. The contributions in this paper are a first step towards designing and empirically grounding such mechanisms.


%\footnote{Damian's work is partially supported by the European Commission grant no. 644869 (H2020 - Call 1), DICE}.
