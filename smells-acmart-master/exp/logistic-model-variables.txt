Unit of analysis: period-stamped file, e.g., MyClass.java on April 1, 2017, MyClass.java on July 1, 2017, each referring to the three months prior to the date 

Dependent Variable: Collection of Boolean values indicating the presence of code smells in a system
		=> The more dependent variables the more models, do we want this?
		=> Do we want to distinguish between different code smells or merely put the presence of a code smell (any code smell) as a dependent variable?

Independent Variables:
	- ID
		1. Filename (anonymised?)
		2. Timestamp/Period

	- Factors that we want to measure
		1. Developer (name?) -- probably not, since there are too many + privacy reasons
		2. Involvement in a community smell (boolean value)
		=> Probably collection of boolean values since we consider multiple community smells.
		=> Here we talk about involvement of *files* in the smells
		3. socio-technical congruence (ratio)
		4. truck-factor 
		=> which definition of truck-factor should we use?  
		5. presence of developers that (a) modified the file during the period of time, (b) has been involved in a community smell during this period
		=> do we want to distinguish between different social smells	
		6. smelly quitters: #individuals that (a) have modified the file during the previous two periods, (b) have been involved in one or mode community smells, and (c) are no longer involved now.
		=> what do we do with periods 1 and 2 where there are not enough previous periods

	- Control for Technical Factors using Basic Code Metrics
		1. CBO (numeric) -- too complex/used in code smell detection?
		2. LOC (numeric)
		3. WMC (numeric) -- too complex/used in code smell detection?
		4. LCOM5 (numeric) -- too complex/used in code smell detection?
		5. Churn (LOC change during the time period)
		6. #commits affecting the file during this period
		7. #total commits affecting the file
		8. #file age - or is this the same as the period (if we start from zero)?
		=> Aren’t code smells detected based on metrics? Wouldn’t our study show a trivial relation between the metrics and the smells based on them?		

	- Control for Developers' and Repository Activities 
		1. # commits of the developer involved in a co-occurrence between a community and code smell (numeric) - not sure
		2. # committers: total number of developers who worked on the file (numeric)
		3. median experience of developers
		=> operationalise as tenure(?)
		4. idem diversity of expertise 
		5. turnover
		6. Ratio core/peripheral that have modified the file during the period

Possible improvements:

	1. As done in CHI'15 paper co-authored by Alexander (http://www.win.tue.nl/~aserebre/CHI15.pdf), we might split projects based on the size of the development team. Indeed, it would be possible that social aspects are more relevant in smaller/larger teams rather than in others;
	=> See that paper on how to distinguish between different time periods (since both social and technical smells are likely to be time-dependent).

	2. Addition of community-specific metrics in the model (e.g., socio-technical congruence or turnover)
	3. Mixed methods :)