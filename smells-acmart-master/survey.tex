% !TEX root = smells-ieeetran.tex
		The \emph{goal} of this study is to elicit possible reasons making developers decide whether to remove code smells, with the \emph{purpose} of understanding if any community-related issue (e.g., a community smell) might influence their decisions. The specific research question targeted with the qualitative study is the following:\smallskip
		
		\begin{itemize}
			\item {\bf RQ1:}  \emph{What concerns affect the developers' decision to eliminate or preserve code smells?}
		\end{itemize}
		
\begin{table}[tb]
	\centering
	\caption{Software Projects in Our Dataset}
	\label{tab:systems}
	%	\rowcolors{2}{gray!25}{white}
	\resizebox{1\linewidth}{!}{
		\begin{tabular}{lcccc}
			\hline
			System & \#Commits & \#Dev. & \#Classes & KLOCs\\ \hline
			Apache Mahout & 3,054 & 55 & 800--813 & 202--204\\
			Apache Cassandra & 2,026 & 128 & 546--586 & 102--111\\
			Apache Lucene & 3,784 & 62 & 5,187--5,506 & 131--142\\
			Apache Cayenne & 3,472&21&2,727--2,854 & 518--542\\
			Apache Pig&  2,432 & 24 & 824--826 & 351--372\\
			Apache Jackrabbit& 2,924&22&842--872 & 473--527\\
			Apache Jena & 1,489 & 38 & 646--663 & 187--231\\
			%Apache Mallet & 5,471 & 34 & 732--736 & 162--201\\
			Eclipse CDT & 5,961 & 31 & 1,404--1,415 & 189--249\\
			Eclipse CFX & 2,276 & 21 & 651--655 & 98--106\\\hline
			\cellcolor[gray]{0.8}\textbf{Overall} & \cellcolor[gray]{0.8} \textbf{32,889} & \cellcolor[gray]{0.8} \textbf{436} & 
			\cellcolor[gray]{0.8} \textbf{546--5,506} & \cellcolor[gray]{0.8} \textbf{98--542}\\
			\hline
		\end{tabular}
	}
\end{table}

\subsection{Context of the Study}\label{sec:contextRQ1}
	The \emph{context} of the study is represented by 117 major releases of 9 large open-source projects belonging to two software ecosystems, \ie \textsc{Apache} and \textsc{Eclipse}. Table \ref{tab:systems} reports the list of systems in the dataset along with their (i) number of commits in the observed time period, (ii) number of developers, and (iii) size as minimum-maximum number of classes and KLOCs in the considered time period. The selection of these systems was driven by our willingness to analyse projects presenting different (a) codebase size, (b) longevity, (c) activity, and (d) population. Starting from the list of projects for the two ecosystems, we randomly selected 9 of them having a number of classes higher than 500, with a change history at least 5 years long, having at least 1,000 commits, and with a number of contributors higher than 20. 
	\revised{We used reference sampling thresholds from literature \cite{TheotokisSKG11,SpinellisGKLASS09}, as they allowed us to focus on large and very active projects having a notable amount of contributors: this is essential to observe the presence of both community smells (very small communities are likely to have less organizational issues) and code smells (small systems contain less code smell instances \cite{palomba2017diffuseness}).}
	
	\smallskip
	As for code smell types, we investigated:
	
	\begin{enumerate}
		\item \emph{Long Method:} a method that implements more than one function, being therefore poorly cohesive \cite{Fowler:1999};		
		\item \emph{Feature Envy:} a method which is more interested in a class other than the one it actually is in, and that should be moved toward the envied class \cite{Fowler:1999};
		\item \emph{Blob Class:} a class usually characterised by a high number of lines of code, low cohesion, and that monopolises most of the systems's processing \cite{Brown:1998};
		\item \emph{Spaghetti Code:} a class without a well-defined structure, usually declaring many long methods \cite{Brown:1998};
		\item \emph{Misplaced Class:} a class that has more relationships with a different package than with its own package \cite{%Fowler:catalogNew,Pietrzak:2005,
		SjobergYAMD13};
	\end{enumerate}
	
The choice of focusing on these smells was driven by the desire to understand how ``eliminate or preserve'' decisions are made for different types of code smells (\eg highly complex classes like \emph{Blob} or source code violating OOP principles like \emph{Feature Envy}) having different granularities (\eg method-level smells like \emph{Long Method} or class-level like \emph{Blob}).
	
\subsection{Detecting Code Smells}
	The first step to answer {\bf RQ1} is concerned with the automatic detection of the code smells considered. To this aim, we relied on \textsc{DECOR} \cite{MohaG07}. The tool uses a set of rules, called ``rule cards''\footnote{http://www.ptidej.net/research/designsmells/}, describing the intrinsic characteristics that a class has when affected by a certain smell type. For instance, the approach marks a class as a \emph{Blob} instance when it has an LCOM5 (Lack of Cohesion Of Methods) \cite{HendersonSellersConstantineGraham:OOS:1996} higher than 20, a number of methods and attributes higher than 20, a name that contains a suffix in the set $\lbrace$\emph{Process, Control, Command, Manage, Drive, System}$\rbrace$, and it has an one-to-many association with data classes. 
	
	\revised{Among the code smell detection tools available in the literature \cite{Fernandes:2016:slr,di2018detecting,mansoor2017multi,palomba2017lightweight,kaur2017support,tahmid2017code}, we selected DECOR because it has been employed in previous investigations on code smells demonstrating good performance in terms of precision, recall, and scalability \cite{KhomhPGA12,FontanaBZ12,Fontana:icstw,Hall:fault2014,Oliveto10-CSMR-ABS,Palomba:TSE15,Palomba:ICPC16}. Overall, \textsc{DECOR} identified 4,267 code smell instances over the 117 considered releases, \ie a mean of $\approx$36 instances per release. A report of the distribution of each code smell type is available in our online appendix \cite{appendix}.}
	
	To verify that the tool was actually suitable for this study, we also validated the performance of DECOR on two of the systems in the dataset, \ie \textsc{Cassandra} and \textsc{Lucene}. In particular, we compared the recommendations provided by the tool with a publicly available oracle reporting the actual code smell instances affecting the systems \cite{Palomba:MSR15}. As a result, we found that the average precision of the tool was 79\%, with a recall of 86\%. Based on these results, we can claim that the selected code smell detector has a performance similar the one declared in previous studies \cite{MohaG07}, being sufficiently accurate for conducting our study.
	
\subsection{\minor{RQ1. Survey Design \& Data Analysis}}
The goal of the survey was twofold: (i) to help practitioners by highlighting the code smells in their code, and (ii) to elicit the data we needed by questioning them on each detected code smell, asking for comments and explanations over that smell, as well as an elaboration over the reasons why the smell was not addressed yet. 

We opt for a  limited, cognitively simple set of questions, to promote responses by reducing the amount and cognitive complexity of questions posed while increasing the developers' immediate benefit (in our case, by raising awareness over a code problem) \cite{Singer:2002,RefWorks:418}. The following list of mandatory questions was selected for our inquiry:

\begin{enumerate}

	 \item \emph{Were you aware of this code smell?} %\smallskip 
	 
	 \item \emph{Can you think of any technical root causes for the smell?} %\smallskip
	 
	 \item \emph{What are the reasons or risks that lead you to decide whether or not to refactor the smell?}
	 	
\end{enumerate}	

	To survey developers that actually have knowledge on social and technical circumstances around a smelly file, we decided to focus on the developers that worked on a smelly class the most (in terms of commits). Thus, we contacted 472 developers that worked with one distinct smelly class instance in any of the releases we considered. 
	\revised{It is worth noting that we excluded developers that worked with more than one smelly class. The rationale here is that developers who worked on several smelly classes might potentially not really be focused on the history of one specific smelly class, \eg they might have confused situations appearing in the context of another smelly class with those of the class we were targeting. To avoid any possible bias, we preferred to be conservative and exclude them from the target population of our survey: all in all, we discarded 168 developers.}

	Being aware of ethical issues commonly associated with empirical software engineering studies, such as confidentiality and beneficence, we adhered to the inquiry guidelines of Singer and Vinson \cite{SingerV02}.
	As such, we prepared an introductory text and clarified the anonymity of their responses. To bootstrap the survey, we used bulk-emailing and email auto-compose tools, posing care in not spamming any participant---every single developer was never contacted more than once. As a result, 162 developers responded out of the 472 contacted ones, for a response rate of 34,32\%---that is almost twice than what has been achieved by previous papers (\eg \cite{Palomba:TSE15,Bavota:tse15,VasilescuPRBSDF15}).
	\revised{In our opinion, there are three aspects that have contributed to this relatively high response rate: (1) we contacted developers that committed the highest number of changes to a smelly class: this means that we only targeted developers who were expert of the considered classes and that might have been more interested in gathering further information on the class they were mainly in charge of; (2) looking at the overall number of commits, we noticed that the involved developers are among the most active in the project; and (3) the time and amount of information required from developers were limited, in order to encourage them to reply to our e-mails.}
	
	\revised{It is worth highlighting that, given the methodology followed to recruit survey participants, we did not collect detailed information on the profiles of our interviewees. For instance, we did not collect data on their programming experience. However, this does not represent a threat in our case. Indeed, we were interested in surveying developers that actually worked on code smell instances, so that we could ask the reasons why they did not refactor them: in this sense, as far as they concretely worked on code smells, it is fine for the type of questions we posed. At the same time, while developers’ experience may have played a role in the answers they provided, looking at the overall number of commits, the involved developers are among the most active in the considered projects: thus, we could assume that they are among the most expert ones for the considered projects.}
	
	
%\subsection{Data Analysis}

	Concerning data analysis, given the exploratory nature of {\bf RQ1}, we applied Straussian Grounded Theory \cite{gt} as follows: (i) \emph{microanalysis}---we labelled survey responses, applying a single label per every piece of text divided by standard text separators (comma, semicolon, full-stop, etc.); (ii) \emph{categorisation}---we clustered labels which were semantically similar or identical, i.e., applying the semantic similarity principle \cite{harispe2015semantic}, a direct consequence of this step is the renaming of labels to reflect categories of labels; (iii) \emph{categories saturation}, i.e., elaboration of \emph{core-categories}---this step entails continued addition of labels to other or new core-categories until no uncategorised label remained; (iv) \emph{taxonomy building}---we represented the tree of categories and labels to visualise the grounded-theory extracted from our survey responses. \revised{Indeed, our choice for Straussian Grounded-Theory is more appropriate for explorative contexts since it does not assume the presence of any previous theory to be tested over the data but rather it adopts a constructivist theory-proving approach wherefore a theory is directly and purely generated from the data. This not withstanding,} to increase inter-rater reliability, two authors independently coded the dataset, subsequently evaluating coding agreement, via the Krippendorff's alpha $\mbox{\it Kr}_{\alpha}$ \cite{krippendorff04}. Agreement measures to 0.87, considerably higher than the 0.80 standard reference score \cite{AntoineVL14} for $\mbox{\it Kr}_{\alpha}$.

\begin{figure}[tb]
\includegraphics[width=0.9\linewidth]{exp/theorynew}
\caption{\revised{A Grounded-Theory of Concerns Affecting the Developers' Decision to Eliminate or Preserve Code Smells, edges on the leaves represent concept frequency counts.}}\label{fig:taxo}
\end{figure}	

\begin{table*}
	%\renewcommand{\arraystretch}{0.8}
	\linespread{0.7}\selectfont\centering
	\caption{Community Smells from our survey, an overview.}\label{smellsoverview}
	{\scriptsize{}}%
	\resizebox{\linewidth}{!}{
		\begin{tabular}{|c|>{\raggedright}p{14cm}|c|}
			\hline 
			\textbf{\scriptsize{}Community Smell} & \textbf{\scriptsize{}Definition} & \textbf{\scriptsize{}\#}\tabularnewline
			\hline 
			{\scriptsize{}Prima Donna \cite{archcomm,TamburriKLV15}} & {\scriptsize{}Repeated condescending behavior, superiority, constant disagreement,
				uncooperativeness by one or few members.} & {\scriptsize{}7}\tabularnewline
			\hline 
			{\scriptsize{}Black Cloud \cite{archcomm,TamburriKLV15}} & {\scriptsize{}Swarming of email or other communication around a new design
				or refactoring exercise - overly complex and disagreeing repeated communication
				obfuscates actual truth.} & {\scriptsize{}15}\tabularnewline
			\hline 
			{\scriptsize{}organizational Silo \cite{archcomm,TamburriKLV15}} & {\scriptsize{}Siloed areas of the development community that do not communicate, except through one or two of their respective members.
			} & {\scriptsize{}1}\tabularnewline
			\hline 
			{\scriptsize{}Lone Wolf \cite{archcomm,TamburriKLV15}} & {\scriptsize{}Defiant contributor who apply changes in the source code without considering the opinions of her peers.} & {\scriptsize{}1}\tabularnewline
			\hline 
			{\scriptsize{}Bottleneck \cite{archcomm,TamburriKLV15}} & {\scriptsize{}One member interposes herself into every interaction across sub-communities} & {\scriptsize{}1}\tabularnewline
			\hline 
			{\scriptsize{}Dissensus \emph{new}} & {\scriptsize{}Developers cannot reach consensus w.r.t. the patch to be applied
				- same condition recurs for other patches in other very complex areas
				of the code} & {\scriptsize{}6}\tabularnewline
			\hline 
			{\scriptsize{}Class Cognition \emph{new}} & {\scriptsize{}The affected class, if refactored, would be made significantly
				more complex to discourage further intervention and introducing a
				massive overhead to newcomers and other less-experienced contributors} & {\scriptsize{}3}\tabularnewline
			\hline 
			{\scriptsize{}Dispersion \emph{new}} & {\scriptsize{}A fix in the code causes a previously existing group or modularised
				collaboration structure in the community to split up or rework their
				collaboration because functionality becomes re-arranged elsewhere} & {\scriptsize{}2}\tabularnewline
			\hline 
			{\scriptsize{}Code Red \emph{new}} & {\scriptsize{}This smell identifies an area of code (a class + immediately
				related ones) which is so complex, dense, and dependent on
				1-2 maintainers who are the only ones that can refactor it} & {\scriptsize{}2}\tabularnewline
			\hline 
		\end{tabular}%{\scriptsize \par}
	}
\end{table*}

\subsection{\minor{RQ1. Analysis of the Results}}
	\revised{Over 80\% of the practitioners admitted being aware of the problems we discovered, sometimes also highlighting that the problems were indeed \emph{well-known} across the community. It is important to note that, although a complementary ``unknown'' category was present, it was \emph{never} applied, since developers were  always aware or well aware of the problems we highlighted. This latter, however, could be due to a confirmation bias---respondents might have felt uncomfortable admitting that they were not well aware of problems in their code.
	The output of our data analysis process, summarizing the evidence from the developer survey, is shown in Figure \ref{fig:taxo}. Generally speaking, developers highlighted the presence of some well-known %technical 
	factors that lead them to avoid refactoring. For instance, the fear of introducing defects while modifying the structure of a system as well as the lack of automated solutions to perform refactoring have been frequently reported by our interviewees. This indicates the presence of important technical ``barriers'' that do not allow developers to promptly improve the quality of source code. At the same time, our participants pointed out some interesting observations that confirm our hypotheses on the role of community smells and, more in general, community-related aspects on the persistence of code smells. In particular, our findings reveal not only that previously known community smells represent an important factor in the refactoring decisional process, but also that there are further community smells that were unknown up to now but that influence the way developers act on code smells. Table \ref{smellsoverview} provides an overview of the smells we observed.}  
		
	\revised{In the following subsections we discuss our findings, focusing on: (a) the community smells that were re-confirmed, meaning that they were previously reported in industry and are re-appearing in open-source as well; (b) newly-emerging community smells, meaning the smells that were never previously observed in industry; (c) other aspects and theoretical underpinnings around community- and technical-related factors.} %The latter aspect, we consider, is especially important not only in the scope of this paper, but also to structure further research in understanding, predicting, and catering for community smells and the connected social debt.

\revised{\subsubsection{Re-Confirmed Community Smells}}
\revised{The first finding that emerged from the survey analysis is that community smells \cite{TamburriKLV15}, \ie symptoms of the presence of social issues within a software development community, represent one important factor leading developers to not spend time in eliminating code smells: 80\% of practitioners explicitly mentioned that avoiding community problems (\eg repeated disagreements) and other social ``smells" \cite{TamburriKLV15}, is the reason why code smells are \emph{not} addressed, meaning that it is more convenient to keep a technical smell than dealing with a community smell.
More specifically, we could confirm the recurrence of five previously known community smells such as \emph{Black Cloud} (mentioned 15 times), \emph{Prima Donna} (2), \emph{Organizational Silo} (1), \emph{Lone Wolf} (1), and \emph{Bottleneck} (1). The participants reported that increasingly confusing information sharing and communication is one of the most prominent reasons why they avoid refactoring (\ie the \emph{Black Cloud} smell). 
Furthermore, repeated uncooperative, condescending, or even defiant behavior with respect to technical or organizational arrangements in a community by a single member (the \emph{Prima Donna} effect) can motivate them to prefer avoiding any type of restructuring for the fear of introducing additional chaos in the community.}

\revised{A smaller number of developers also reported how the presence of sub-teams that do not communicate with each other (the \emph{Organizational Silo}) or the absence of communication with one of the members who prefer working independently from the others (the \emph{Lone Wolf}) can explain their refactoring decisions. Finally, the absence of flexibility in the community---indicated by the presence of a member that tries to interpose herself into every formal communications (the \emph{Bottleneck})---make that developers are not always aware of the design decisions made by other people in the community and, for this reason, they sometimes avoid restructuring to not introduce defects and/or worsening the overall program comprehensibility.}

\revised{An interesting example of the discussion made so far is presented in the quotation below, where a developer commented on an instance of \emph{Long Method} that was not refactored:}\smallskip

\begin{quotation}
\noindent \revised{\emph{``We are aware that this code is problematic, but we have neither time and tools to correctly perform a splitting. Furthermore, we have two subteams working on it, and the communication with the other subteam is not good.''}}\smallskip
\end{quotation}

\revised{Besides explaining that the lack of tools and time are important factors in the decisional process, the developer clearly pointed out the presence of an \emph{Organizational Silo} that involves two sub-teams that cannot properly communicate with each other. As a consequence, the developer preferred to avoid any type of pervasive modification which may have led to introduce additional problems. All in all, the results presented and discussed so far can already confirm our conjecture: community smells can influence the persistence of code smells.}

\revised{\subsubsection{Newly-Emerging Community Smells} }
\revised{Besides the five known community smells, our data indicates the existence of 4 \emph{previously unknown} community smells recurring at least twice in two different projects, and reported by two different developers. For example, we discovered in 3 different projects that developers repeatedly manifested a previously unknown \emph{Dissensus} community smell, namely, inability to achieve consensus on how to proceed despite repeated attempts at it---as a consequence, the code smell was kept as-is. For instance, a developer reported that:}\smallskip

\begin{quotation}
\noindent \revised{\emph{``Yes, we know this problem. But every time we talk about it, we are not able to find a common solution.''}}\smallskip
\end{quotation}

\revised{Note that this smell is not completely unknown in organizational literature: indeed Bergman \etal \cite{bergman2012shared} indicate that social conflict is associated with reduced productivity and inability to reach consensus.} 

\revised{Our results also indicate that in all projects targeted by our survey, practitioners often did not refactor code smells since refactoring would cause a previously unknown \emph{Class Cognition} community smell, namely, that refactoring would cause the modular structure and refactored classes to be more difficult to understand and contribute to \cite{ammerlaan2015old}, \eg for newcomers. This is the case of a developer who analyzed a \emph{Feature Envy} instance reporting:}\smallskip

\begin{quotation}
\noindent \revised{\emph{``Generally I try not to perform re-organization of the code that implies the modification of the location of code components. This because (i) other developers could waste time and effort in understanding the new environment of the method, and (ii) I cannot simply identify a suitable new location for the code.''}}\smallskip
\end{quotation}	

\revised{Thus, she indicated that the re-location of the method could have caused comprehensibility issues to other developers. The two smells discussed above were \emph{intuitively, but precisely} described by 6 and 3 distinct developers. In addition, we revealed the existence of the \emph{Code-red} community smell, that denotes the existence of extremely complex classes that can be managed by 1-2 people at most. As an example, one of the participants who analyzed a \emph{Blob} instance explicitly reported that:}\smallskip

\begin{quotation}
\noindent \revised{\emph{``Totally un-understandable code is difficult to touch. I modified this class only for fixing a potential bug, but generally only 1 or 2 devs can substantially modify it.''}}\smallskip
\end{quotation}	

\revised{Finally, we found the \emph{Dispersion} community smell, which concerns a fix or refactoring that caused a previously existing group of modularised collaboration to fragment and work haphazardly because of functionality rearrangements. In contrast to the \emph{Class Cognition} community smell, this smell has nothing to do with code understandability \cite{ChhabraAS03} to newcomers, but rather it refers to making normal maintenance activities in the community more difficult to carry out and coordinate. To better explain the nature of this smell, let us consider the following quote from one of our surveyed developers:}\smallskip	

\begin{quotation}
\noindent \revised{\emph{``If the algorithm implemented in the method would be split, then the developers working on that code would become crazy since they are able to work pretty well on the existing code.''}}\smallskip
\end{quotation}	

\revised{In this case, the developer was analyzing a \emph{Long Method} instance but they did not proceed with an \emph{Extract Method} refactoring in order to avoid the risk of other team members losing their knowledge on the algorithm implemented in the method, threatening its future reliability.}

%\FABIO{@Damian, can you please better explain the different between Class Cognition and Dispersion?}

\revised{In conclusion, we can observe how all the newly emerging smells are socio-technical, \ie blend together social and technical aspects, which confirms the need for further quantitative analysis and exploration of the mutual relation between code and community smells. It is worth mentioning that developers were not made aware of the notion of community smells and spontaneously, intuitively expressed the repeated community characteristics causing or relating to code smells---this intuition, feeling of unease, is by definition the indication of a community smell \cite{TamburriKLV15}.}

\revised{\subsubsection{Additional Aspects Influencing Refactoring Decisions}}\label{cause} 

\revised{While most of the developers directly pointed out community smells as one of the main reasons leading them to avoid refactoring of code smells, some participants also indicated the existence of additional aspects that impact on their refactoring decisions. Specifically, our data indicates that one of the most common reasons to avoid refactoring is the fear of (i) wasting time or (ii) the technical consequences of this action. Specifically, 7 developers pointed out the risks to introduce new defects while performing refactoring, thus confirming the findings by Kim \etal \cite{kim2014empirical}, who reported that developers do not think of refactoring as a behavior-preserving activity and, as a consequence, it may introduce new defects in the codebase. At the same time, 6 developers identified the lack of trust in the refactoring tools as the main cause to not remove code smells - this is, again, in line with previous findings in the field \cite{kim2014empirical,murphy2012we}. Interestingly, one developer reported that a co-occurring aspect to consider when removing a code smell is whether the class also contains a clone: in this case, the refactoring would be much more costly as other code clones should be checked for the presence of the smell and eventually refactored.} 

\revised{Still in the context of technical factors, $\approx10\%$ of the respondents elaborated on the perceived technical mediators for unresolved code smells, pointing to well-known \emph{architectural reflection} phenomena, such as architecture erosion or architecture drift \cite{kazmanbook}. They also pointed out that a high number of dependencies toward other classes can be an important reason to avoid a refactoring action.}

\revised{Furthermore, developers are often scared of one key \emph{contingency}, that is, modifying classes which are subject of both code and community smells---refactoring these classes is avoided or limited to \emph{conservative-fix} only. Finally, our data also indicates that developers devised \revised{a new \emph{maintenance device}} to address classes which carry a strong indication of code and community smells, besides re-organisation and re-modularisation. On the one hand, community smells exist at the boundary of people and code, i.e., they are patterns which include both a people and a code component. On the other hand, developers reportedly used \emph{organizational commenting} within code, that is, including maintenance and evolution instructions in source code comments such that, for example, newcomers can contribute knowing what to touch and what not to modify at all.}

\revised{In conclusion, our main results from the analysis of the additional factors influencing the persistence of code smells show that (i) fault-proneness, (ii) lack of tools, (iii) co-occurrence of code clones, and (iv) coupling of a class are the main technical factors explaining the willingness of developers to perform refactoring.}\medskip
	
\subsection{Summary of Findings}
\revised{In summary, the main output of our qualitative analysis revealed that the decision on whether to refactor a code smells is dependent on a number of different factors. It is indeed not only dependent on community or technical factors, but rather their \emph{combination} better fits the developers' willingness or ability to maintain code smells. This seems to indicate that \emph{community-aware} code smell prioritisation approaches could better pinpoint to developers which code smells can be more easily removed, thus providing a more practical solution to deal with them.}

\revised{The results also provide a clear indication that community and code smells are influenced by each other. 
\revised{We adopted the \emph{Timeliness::Social-Then-Technical} code to responses of developers saying that they did not address a code smell because it would cause a community smell---the community smell is then the effect of the code smell.} 
Conversely, the opposite is true for the \emph{Timeliness::Technical-Then-Social} code. 
Through content analysis we observed that, for over 70\% of the reported code smells, the decision not to refactor was due to a potential community smell, i.e., \emph{Timeliness::Social-Then-Technical}. This evidence seems to indicate a dimension of intentionality for code smells themselves---oftentimes it is more convenient to keep code smells rather than addressing community smells. This result is particularly important, as it suggests the need for practical solutions aiming at \emph{anticipating} situations that might become critical for persistence in the next future.}
		
  \begin{center}
	\begin{examplebox}
		\textbf{Summary for RQ1.} \revised{Our evidence shows that community smells, together with other technical factors, % (\eg coupling),
		 influence the maintenance decisions for code smells. At the same time, we observed that in several cases it is more convenient to keep code smells rather than addressing community smells. These findings suggest the need for (1) \emph{community-aware} approaches for assessing the refactorability of code smells and (2) automated ways to anticipate critical situations that may lead developers to not refactor a code smell at all.}
	\end{examplebox}
\end{center}



