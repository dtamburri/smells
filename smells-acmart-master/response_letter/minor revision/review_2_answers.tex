\section*{Answers to Reviewer \#2}

\quotation{
The paper presents an interesting study that indicates four new community smells and the influence of different factors on the intensity of  code smells. Please find below some questions and suggestions. 	
}

\answer{We would like to thank the Reviewer for the provided feedback that were instrumental in improving our work. We tried to address all of the comments as detailed below.
	
\textcolor{blue}{To facilitate the new round of review, we have highlighted in blue in the manuscript the main changes/additions (i.e., we did not highlight minor fixes and writing issues).}}

\quotation{Title\\
I did not understand the relation of this part ``There are Other Fish in the Sea!'' with the study.
} 

\answer{Up to now, to the best of our knowledge, the research on code smells mainly focused on the understanding of what are the technical aspects of source code that, e.g., can indicate the presence of design issues, can be perceived by developers, can amplify their effects, can be used to understand under which conditions developers perform refactoring, etc. Thus, as a matter of fact there is no study yet that investigates whether and what are the community-related factors influencing code smells. We captured this lack of studies and came up with this title, that aims at highlighting how there are other aspects to take into account in the enormous research field that is code smell detection and management, i.e., there are other fish in the large sea. For the mentioned reasons, we would like to keep the title as is; however, if the Reviewer really wants us to avoid this kind of metaphors, we will change the title.  
}

\quotation{
Abstract\\

- What is a ``sub-optimal'' pattern of code? The term ``sub-optimal'' needs to be properly defined. \\
Similarly, what is a ``sub-optimal'' organisational and socio-technical characteristic? 
}

\answer{	
The Reviewer is right. The term ``sub-optimal'' pattern of code refers to circumstances where developers decide to set apart good design and implementation principles in order to deliver faster a new feature or when fixing a defect. Similarly, the concept can be tailored at an organizational level: patterns across the organizational structure around a software system that may lead to additional unforeseen project costs (e.g., condescending behavior, disgruntlement or rage quitting) are those that we consider as ``sub-optimal''.
	
In the revised paper, we substantially improved the abstract to clarify the meaning of the term sub-optimal. Furthermore, the newly included Section 2, now contains terms and definitions.	
}

\quotation{
- It is not clear the motivation to propose a code smell intensity prediction model. 
}

\answer{
Thanks for sharing this potential issue. We see four main motivations for having a code smell intensity prediction model, as reported in the following:

\begin{enumerate} 
	
	\item As correctly said by the Reviewer 1, a code smell that co-occurs with a community smell may be not taken into account by a developer. While this can be true, the role of prediction models is indeed that of anticipating situations where the co-occurrence might lead to more serious maintainability issues. For example, suppose the case in which, in a certain release R$_i$, a code smell has a ``LOW'' intensity and that our prediction model predicts the intensity of this instance to be ``HIGH'' in the subsequent release R$_{i+1}$ because the features of the model related to community smells make to model prone to forecast an important decrease of the overall maintainability of the affected class. In this case, a project manager may immediately take action, trying to fix the community-related issues with the aim of preventing the smell to increase in its intensity. This is the key use case scenario of our work and we honestly believe on its practical usefulness and actionability.
	
	\item As shown by recent papers \cite{PalombaBPOL14,taibi2017developers}, the perception of code smells heavily depends on their intensity. We argue that approaches able to precisely indicate the future severity of code smell instances might allow developers when (i) understanding the possible criticisms of the software program being developed that may arise in a short-term future and (ii) dealing with or simply monitoring their evolution \cite{palomba2017scent}.
	
	\item In the past, we have shown that the intensity of code smells has a strong impact on fault-proneness \cite{palomba2017diffuseness} and can be actually used to identify parts of source code that are likely to be defective \cite{palomba2017toward}: thus, intensity prediction models can help developers assessing when a certain refactoring or other program transformations must be applied to not incur in possible additional maintainability and/or reliability problems.
	
	\item Intensity can be used as a means for selecting the code smell instances that need to be more urgently fixed. As not all the smells are removed, an approach able to rank them based on their severity might be worthwhile to allow developers selecting the instances on which to focus more, or even those that are more relevant to manage because of the co-occurrence of a community smell.

\end{enumerate}

For all these reasons, we strongly believe that the proposed prediction model makes sense and can be immediately actionable in practice. At the same time, these reasons represent the main glue between the two studies of our paper: indeed, on the one hand we raise the need for taking into account community-related factors while assessing source code maintainability; on the other hand, we also show how this is possible, i.e., how to exploit community-related information to devise a prediction model that can help practitioners in preventively fix possible issues during software maintenance and evolution. 

In the revised version of the paper, we added a new recap in Section 4, where we reported rationale and motivations why the definition of a code-smell-intensity prediction model makes sense. 	
}

\quotation{
- Regarding this part ``community-related factors are perceived, either by most developers in our qualitative survey or by our quantitative statistical model, as the causes of the persistence of code smells.'', note that the results indicate that community-related factors have an influence in the presence of code smells but they are not the main causes. The findings of the RQ2 Indicate that the technical factors still give the major explanation of the presence of code smells.
}

\answer{
The Reviewer is right. As correctly pointed out, community-related factors represent a co-occurring phenomenon rather than the main cause preventing developers to perform refactoring. In the revised paper, we toned down the discussion in Section 3 to better reflect our findings. 
}

\quotation{
Introduction\\
- Regarding this part ``The social interactions among the involved actors may represent the key to success as well as a critical issue possibly causing the failure of the project [21], [91].'', I am not sure if [21][91] support this affirmation.  
}

\answer{
Fixed. We rephrased the sentence to address the actual meaning we were implying, that is, additional socio-technical and organisational costs ensue if certain social interactions and the actions behind them are not accounted for and controlled.
}

\quotation{
- What is a ``sub-optimal'' development community? What is the difference between an ``optimal'' and ``sub-optimal'' development community? 
}

\answer{
Much like a sub-optimal piece of code which carries a code-smell, a sub-optimal software development community is riddled with community smells or other social debt indicators (e.g., low engagement among members or extremely high turnover). Conversely, an optimal community is best-fit for its purpose and is ridden of said community smells - the Reviewer should note that, on one hand, not all the social debt indicators are yet known and, on the other hand, this paper operationalises a mere 4 smells, with a potential of over 50 discovered in the state of the art of organisations and social networks research. Much more work is needed in this direction; we are planning several spin-offs of this work and in that respect we aim to reach a general theory of social debt for improved software engineering practice, maintenance and evolution.
}

\quotation{
- According to the authors, ``community smells'' represent ``social debts'' that are related to the ``unforeseen project cost connected to a sub-optimal development community''. However, the study does not deal with ``cost''. 
}

\answer{
We agree, we have likely not properly defined community smells. Such smells emerge when the development community does not act in a cohesive manner, possibly leading to an increase of costs (e.g., in case of missing communication between developers that cause delays in addressing change requests). Thus, the increase of costs is a consequence rather than the cause: we made sure to improve the definition of community smells in the revised manuscript. Specifically, we added a new section called ``Terminology'' in order to explicitly present all the terms used in the context of our paper (the new Section 2).
}

\quotation{
- The authors define ``social debt'', but they do not define ``technical debt''. 
}

\answer{
Correct. We defined it in the new Section 2.
}

\quotation{
- Regarding this part ``Our empirical investigation features a convergence mixed-methods approach [23], [49], [28] (see Fig. 1) where quantitative and qualitative research are run in parallel over the same dataset with the goal of converging towards a theory.'', the goal is to converge towards a ``theory''. But, it is not clear the motivation to create this theory neither the kind of theory to be created.  The authors do not describe the process adopted to achieve the theoretical saturation. 
}

\answer{
We thank the Reviewer for this comment since it reflects a shortcoming we did not previously notice. To address this point, we rephrased the brief outline of our methods in the introduction to highlight that theoretical saturation for our research was originally designed to be achieved via mixed-methods convergence; our assumption was that if both qualitative and quantitative data led to the same conclusions, then our theory would be correct. We provide a better explanation of this point in the rephrasing.
}

\quotation{
- The study involved 162 developers but the authors do not describe in details the perfil of these developers. 
}

\answer{
We see the Reviewer's concern. Unfortunately, we do not have enough data to provide a comprehensive overview of the developers' profiles. Nevertheless, we believe this is not a critical problem in our case because of two reasons. In the first place, we were interested in surveying developers that actually worked on code smell instances, so that we could ask the reasons why they did not refactor them: in this sense, as far as they concretely worked on code smells, it is fine for the type of questions we posed. In the second place, while developers' experience may play a role here, it is important to note that looking at the overall number of commits, the involved developers are among the most active in the considered projects: thus, we can assume that they are among the most expert ones for that projects. Moreover, as a final note, 95\% of them (i.e., 154/162) still committed changes in the time given for this major revision, meaning that they continue to be extremely active now. 

In the revised manuscript, we clarified these points in Section 3.3 and further discussed them in Section 6.
}

\quotation{
- The study aims at investigating ``how community smells influence the maintainability of code smells'' but the authors do not define which kind of maintainability activities will be considered. According to RQ1, I consider that the maintainability activities are related ``to eliminate or preserve code smells'', correct? 
}

\answer{
Correct. We aim at studying how community smells influence the intensity of code smells, we clarified in the new version of Section 1.
}

\quotation{
- The authors do not motivate the creation of a ``novel code smell intensity prediction model''. Several studies have been proposed to predict code smells. Are they not useful? 
}

\answer{
They are partially related to this study. Most of the code smell prediction models defined so far aim at identifying the presence of code smells rather than assessing their future intensity. Thus, they have a different goal than the model we proposed. Nevertheless, the Reviewer is right and we discussed them in Section 7, ``related work'' of the revised paper.
}

\quotation{
- According to the authors, ``the results indicate that the code smell intensity prediction model built using community smells is able to more accurately predict the future intensity of code smells than a model that does not explicitly consider the status of software communities''. However, the statistical significance of this comparison was not verified. 
}

\answer{
Actually, we verified the statistical significance of our results through the Scott-Knott test: as explained in Section 4.3.5, we adopted this test to statistically verify the differences in the AUC-ROC values obtained for the different models. We better clarified this point in the revised manuscript in Section 4.3.5.
}

\quotation{
- The authors reveal the existence of 4 community smells, but it is not clear the theoretical saturation process that was used to obtain these four new kinds of smells. 
}

\answer{
We recognise a certain degree of confusion around this point. To clarify, the 4 community smells detected in the quantitative part were already previously known in the state of the art. With the qualitative study we aimed at: (1) confirming their presence at large; (2) connecting their presence with cause-effect relations over code smells. At the same time, with the quantitative part we aimed at: (1) revealing the co-occurrence of code and (previously known) community smells; (2) looking at the effect size of detection power by including community smells in said detection power. We succeeded in achieving theoretical saturation for all of the points above either by: (a) using Grounded-Theory, on a qualitative sense; (b) using a quantitative approach and statistical modelling, on a quantitative sense. To address this clarificatory remark, we added a research objectives digest in the contributions area of the intro section.
}

\quotation{
- The authors need to properly define which means ``top'' community-related factors. 
}

\answer{
The reviewer makes a very valuable point. We fixed the issue by clarifying what we originally meant with ``top'', removing the hyperbole and providing explanatory text in the newly added second-last paragraph of the introduction.
}

\quotation{
- I could not identify the replication package. 
}

\answer{
We explicitly link the replication package in Section 1 (Contributions and Implications) of the new version of the paper. 
}

\quotation{
Sections 2-6\\
- Regarding these numbers: ``we randomly selected 9 of them having a number of classes higher than 500, with a change history at least 5 years long having at least 1,000 commits, and with a number of contributors higher than 20, using reference sampling thresholds from literature [3], [89].''; Why are these numbers relevant to this study? For example, why select project with a change history at least five years is relevant to this study,  the study does not consider the temporal activities of the projects.  
} 

\answer{
We used reference sampling thresholds from literature \cite{TheotokisSKG11, SpinellisGKLASS09}, as they allowed us to focus on large and very active projects having a notable amount of contributors: this is essential to observe the presence of both community smells (very small communities are likely to have less organisational issues) and code smells (small systems contain less code smell instances \cite{palomba2017diffuseness}).

In the revised paper, we better clarified the motivations behind the selected filters (see Section 3.1).
}

\quotation{
- The authors analysed five smell types: Long Method, Feature Envy, Blob class, Spaghetti Code and Misplaced Class. But, they do not describe the number of smells existing into each analysed project. 
}

\answer{
Correct. We added this information in Section 3.2.
}

\quotation{
- Regarding this affirmation ``...among all the code smell detection tools available in the literature [31]...'',  the authors cite only [31]. Even though [31] presents a study of existing tools, such [31] was published in 2016, other code smell detection tools have been proposed. 
}

\answer{
Thanks, we verified the existence of five new detectors defined between 2016 and 2018, and complemented the list of citations with the newly found ones. 
}

\quotation{
- The first question used by the authors in the survey is: ``were you aware of this code smell?''. In such question, the authors do not consider the perception of the developer if he agrees or not with the existence of the smell. This kind of question (``were you aware of this code smell?'') may introduce a ``bias'' to the study. 
}

\answer{
It is correct that we did not consider whether a developer considered a smell as an actual design problem; thus, it is possible that the decision to not refactor some of the instances was driven by their lack of perception. However, we tried to keep the survey as short and quick as possible in order to stimulate developers to answer. For this reason, we limited it to the essential questions needed to address our research question. At the same time, it is important to note that the developers were free to say that the piece of code was not affected by any smell: while this does not avoid potential confirmation biases, the fact that some developers explicitly reported the absence of an actual design problem in the analyzed code (this happened 3 times) partially indicate that the involved developers were not biased when answering the survey. In any case, we recognized this potential issue in Section 6 of the revised paper. 
}

\quotation{
- The authors do not describe the reasons to adopt the ``Straussian Grounded Theory''. Why is this theory more appropriate to the proposed study? 
}

\answer{
The Reviewer makes a valuable observation here. Indeed, Straussian GT is more appropriate for explorative contexts since it does not assume the presence of any previous theory to be tested over the data but rather it adopts a constructivist theory-proving approach wherefore a theory is directly and purely generated from the data. We added explanatory text where we introduce the research design choice (see Section 3.3).
}

\quotation{
- The theory indicates the existence of 4 previously unknown community smells. However, the authors do not describe the process used to achieve the theoretical saturation. 
}

\answer{
We are not sure as to the details that the Reviewer is seeking; according to our understanding of the literature, theoretical saturation is the phase of qualitative data analysis in which the researcher has continued sampling and analyzing data until no new data appear and all concepts in the theory are well-developed; the process we followed to achieve such saturation is described in detail in Section 3.3. Of course if we misunderstood the level of detail that the Reviewer is seeking we would be happy to revise the section further.
}

\quotation{
- The authors use a variety of terms without defining them properly, for example, technical debts and social smells.
}

\answer{
We better defined technical code smells and community smells in the revised submission. Precisely, we included the explicit definitions in the new Section 2.
}

\quotation{
- In ``Summary of RQ1'', the authors affirm that community smells ``strongly'' relate to code smells. \\
- Which means ``strongly'' in the context of this study? Did the authors use some statistical test to verify how strong is this relation between community smells and code smells? 
}

\answer{
No, we did not apply statistical tests: we used the term ``strongly'' because many developers pointed out the presence of community smells as a cause for not refactor code smells. However, we agree with this comment, and for this reason in the revised paper we avoided the use of the term in the summary for RQ1 and made sure to not use similar terms in cases where we could not properly use them.
}

\quotation{
- The community smells proposed by the authors were not used in the analysis performed to answer the RQ2 and RQ3. Thus, we do not know if these kind of smells are relevant factors to the increasing of code smell intensity. As a consequence, I am not sure if these factors are relevant to the researches in the context of code smells. 
}

\answer{
The relation between first and second study has been one of the major issues encountered by all the Reviewers, and we thank them for pointing out this important problem of the previously submitted manuscript. In the former version of Section 2 we did not report (or we only rapidly mentioned) an important filtering we did when analyzing the survey data: specifically, in Figure 2 and in the related discussion we only reported the codes which developers referred to at least twice, i.e., we did not report the reasons to not refactor that were mentioned by at least two developers. We are sorry for not having reported the entire set of answers provided by them.

These answers are of a paramount importance because actually make the relation between the two studies even clearer than what explained when answering to the first comment of the Reviewer. In particular, the previously discarded answers provide evidence that the three community smells considered in the second study but not discussed in the first one, i.e., Organizational Silo Effect, Lone Wolf, and Bottleneck, might still represent causes behind a missing refactoring action aimed at removing code smells. 

For instance, one participant reported that the reason why she did not refactor a Long Method instance was because ``I'm the only working on it and I don't care of whether the others believe I should do it''. This answer was coded as ``Lone-Wolf'' in our dataset but, unfortunately, not reported because of the aforementioned filter. The same happened with the other two community smells. 

In the revised manuscript, and in line with comments from Reviewer 1, we addressed this major issue as follows:

\begin{enumerate}
	\item We reported *all* the available survey data, improving the discussion of the results (Section 3.4) as well as the summary of findings reported in Section 3.5;
	
	\item We substantially improved Section 3.1, which reported the context of the second study, with the aim of making explicit that all the community smells investigated in the second part of the paper are related to the first one.
\end{enumerate}
}

\quotation{
- The authors used the CodeFace4Smells tool to detect community smells and they evaluated this tool on 60 open-source projects. But, it is not clear if the projects analysed in the proposed study are included in these 60 projects. Otherwise, we can not consider that the accuracy of the CodeFace4Smells is applied to the projects analysed in the proposed study. 
}

\answer{
Good point. Yes, the projects used in this paper were also involved in the \textsc{CodeFace4Smells} evaluation. We clarified this in the revised paper (see Section 4.2). For the sake of completeness, we also confidentially link and put in our online appendix the paper reporting the evaluation of the community smell detector (currently under major revision in IEEE Transactions on Software Engineering). 
}

\quotation{
- The authors measured the ``final intensity'' as the ``mean'' of those normalised scores. However, the ``mean'' can be strongly influenced by outliers. Thus, it is not a good measure to be adopted. 
}

\answer{
As explained in the paper, when computing the intensity of code smells we followed a procedure already applied in previous work. However, we can agree with the Reviewer's comment: to verify the extent to which the mean was a meaningful indicator in our case, we completely re-ran our study computing the final intensity using the median operator rather than the mean. As a result, we did not observe differences with respect to the results previously reported. Thus, we can conclude that in our case this choice did not influence our findings. 

In the revised paper, we explicitly discussed this point in Section 6, while we put the detailed results of this additional analysis in our online appendix.
}

\quotation{
- Why did the authors adopt the quartiles to assign the intensity to one of the classes (low, medium...)? 
}

\answer{
Quartiles are classical methods for measuring the skewness of data as is in our case; we simply chose to map each quartile to an individual class (low, etc.) thus making our research design void of any misinterpretation from a statistical perspective. We clarified our choice in Section 4.3.1.
}

\quotation{
- Regarding this part ``...For this reason, based on findings of previous literature ..., we defined a list of technical factors...'', it is not clear which findings the authors considered to define the list of technical factors. 
}

\answer{
Thanks for this comment. In the revised paper, we improved 4.3.3 in order to clearly point to the exact findings used to derive the list of technical control factors employed. 
}

\quotation{
- It is not clear the motivation to consider ``code clones'' and ``fault-proneness''. In addition, the effectiveness of the tools [46,27] cannot be considered to the projects analysed in the study since these tools were evaluated in other projects. 
}

\answer{
We included those factors because several developers in previous pilot studies and informal conversations reported them as a potential reason not to maintain a smell is the error-proneness of the class, while an equal number of developers explained that code clones could influence them when deciding on refactoring actions. We better clarified these points in the revised version of the paper.

As for the accuracy of the tools, we agree with the Reviewer. While a comprehensive evaluation of the tools is not doable because of the lack of publicly available datasets reporting the defects and clones present in all the considered systems (the definition of such datasets would require specialized methodologies that go out of the scope of this paper), we addressed the Reviewer's concern as explained in the following.

In case of the DCBM defect prediction model, we could test its performance on two of the systems included in the study, i.e., Lucene and Pig. Indeed, the study conducted by Di Nucci et al. \cite{di2017developer} comprises these two systems and therefore a ground truth reporting the actual defects contained in their releases is available. Thus, starting from the publicly available raw data, we ran the model and evaluated its F-Measure and AUC-ROC. The results indicated that on Lucene the F-Measure was 83\%, while the AUC-ROC reached 79\%; in the case of Pig, instead, the F-Measure was 86\% and the AUC-ROC was 78\%. Note that the percentages were computed considering the median values obtained by running the model over all the considered releases. Of course, we cannot ensure that such performance holds for all the systems considered, however our additional analysis makes us confident of the fact that this approach can be effectively adopted in our context. It is important to remark that our results are in line with those reported by Di Nucci et al., but they are not the same. This is due to the slightly different validation strategy: while they trained and tested the model using a 3-months sliding window, we needed to perform a release-by-release strategy. This implied training and testing the model on different time windows. We see some additional value in this way to proceed: we could not only confirm previous results, but as a side contribution we provide evidence that the findings by Di Nucci et al. hold with a different validation strategy. 

With respect to \textsc{DECKARD}, as explained we could not extensively evaluate it. Nevertheless, during the major revision we assessed its precision on the same set of systems considered for the defect prediction model, i.e., Lucene and Pig. Specifically, we ran the tool over each of the considered releases and manually evaluated whether its output could be considered valid or not. To avoid any sort of bias, we asked to an external industrial developer having more than 10 years of experience in Java programming to evaluate the precision of \textsc{DECKARD} for us: the task required approximately 16 work hours, as the tool output was composed of 587 candidate clones. As a result, 439 of the candidates were correctly identified by \textsc{DECKARD}, leading to a precision of 75\%. Also in this case, we believe that the tool has a performance reasonably high to be used in our context. 

In the revised version of the paper, we discussed on the evaluation of the tools in Section 6.
}

\quotation{
- There are a variety of techniques to evaluate the relevance of features to predict a dependent variable. Why did the authors use the ``information gain'' technique?
}

\answer{
Basically, we opted for Information Gain because we can quantify the actual gain provided by a certain feature to the performance of the model. This would not be possible with other techniques. For instance, the Wrapper technique \cite{kohavi1997wrappers} is pretty popular to evaluate the relevance of the features of a prediction model  (e.g., Di Nucci et al. \cite{di2017developer} used it to select features of their model): despite its power, it just outputs the subset of relevant features without providing an indication of how much they are important for the model. Thus, to summarize, our choice was driven by the possibility to provide fine-grained insights on the entropy reduction of each considered feature. In the revised manuscript, we clarified this point in Section 4.3.5.
}

\quotation{
- Several studies have been evaluated the effectiveness of machine learning techniques to predict code smells; it is not clear the reasons to adopt the multinomial regression technique. The authors adopted it because [25,40] indicate that the multinomial regression technique is the most reliable ones. But, [25,40] do not perform a deep evaluation of the reliability of machine learning techniques.
}

\answer{
This is a good point, thank you. We perfectly agree with the Reviewer: for this reason, we performed an additional analysis aimed at evaluating whether the Multinomial Regression classifier is actually the one performing better. In particular, before selecting the classifier to use in our study, we compared Multinomial Regression with 6 other techniques, i.e., ADTree, Decision Table Majority, Logistic Regression, Multilayer Perceptron, Support Vector Machine, and Naive Bayes. We selected these approaches because they were previously used in the context of code smell prediction: as a result, we could actually confirm the superiority of  Multinomial Regression, which achieved an F-Measure 8\% higher with respect to Support Vector Machine, namely the classifier which performed the best after Multinomial Regression.

In the revised paper, we discuss this point when explaining the classifier selection (Section 4.4), while we reported the detailed results of our analyses in the online appendix. 
}

\quotation{
To evaluate the effectiveness of the code smell intensity prediction model, the authors used an inter-release validation procedure. Besides of this procedure, it is necessary to consider a n-fold cross-validation in order to increase the confidence of the results. 
}

\answer{
We thank the Reviewer for this comment, however we are not sure that we can really apply an n-fold validation in our case or, at least, we are not sure that the ``meaning'' of the evaluation would still be the same. In our study, we deal with historical data coming from different releases: in this case, there are two possible ways in which a n-fold validation could be applied. 

\begin{enumerate} 
	\item In the first option, data of all the releases is merged together before running an n-fold validation where n is represented by the total number of releases of a certain project: however, this would lead to a totally wrong and misleading validation, as it would be possible to train the model with data coming from releases that are subsequent with respect to the data in the test set. In other words, we could not preserve the temporal relationships between different releases. Thus, we could not really perform it.

	\item In the second option, an n-fold (e.g., 10-fold) validation could be applied on each release independently and then average/median operations could be used to interpret the overall performance of the model on a certain project. While this strategy could be theoretically implemented, the results would not be comparable with those reported in our paper. Indeed, on the one hand we would have a set of independent classifications that are time-independent; on the other hand, an evaluation done explicitly exploiting temporal relations, i.e., a time-dependent analysis. Thus, even if we would have done this analysis, it would have not increased at all the confidence of the results, but rather created confusion. Given the nature of our analysis and the need to perform a time-sensitive analysis, we believe that our validation strategy is the best option in our case.
\end{enumerate}

For these reasons, we have preferred to not implement any major modification for this comment in the revised version of the manuscript. However, we discussed this point in the threats to validity (see Section 6).  
}

\quotation{
- The authors need to describe in more details the design and results of the experiment involving the five industrial project managers.
}

\answer{
We agree, thanks for this comment. We included more details on this confirmatory study in Section 4.4. 
}

\quotation{
- In general, the proposed model prediction was not able to reach effectiveness above 0.8. But, studies indicate that machine learning techniques can reach effectiveness above 0.9 on detecting code smells. My question is: is the proposed prediction model relevant when compared to the literature?  
}

\answer{
We strongly believe so. The main reason is that most of the prediction models defined so far do not aim at predicting the intensity of code smells, but rather their existence. This means that the effectiveness reached by those models when detecting code smells cannot be directly compared with the one defined herein since they are two different classification problems. 

Furthermore, we believe that our model is even more relevant because we explicitly take into account factors that, as shown in the qualitative study with developers, are highly important to properly assess the maintainability of code smells. 
}

\quotation{
- The main goal of the RQ3 is to evaluate if community-aware code smell intensity prediction model improve the performance of models that do not consider this information. But, the authors do not show if exist a statistically significant difference between these approaches. In such cases, the authors can use statistical tests, such as Wilcoxon.  
}

\answer{
Actually, we performed a statistical analysis of our results by relying on the Scott-Knott Effect Size Difference test. As we needed to perform a statistical comparison of different models over multiple datasets, the use of more popular statistical techniques like, for instance, Wilcoxon or Cliff's Delta is not recommended because they might lead to inappropriate interpretation of the results or even wrong application of statistical tests \cite{demvsar2006statistical}. In situations like these other statistical tests should be applied: Scott-Knott ESD represents one of the tests that can be safely applied and that was already successfully adopted in previous software engineering research (e.g., by Tantithamthavorn et al. \cite{tantithamthavorn2016automated}). Nevertheless, as noticed by Reviewer 3, it is a variant of the original Scott-Knott test defined by software engineering researchers \cite{tantithamthavorn2016automated} and, therefore, not belonging to the standard statistical literature. For this reason, we decided to change the statistical approach in the revised manuscript: specifically, we applied the classical Scott-Knott test \cite{ScottKnott}. This test is widely used as a multiple comparison method in the context of analysis of variance \cite{gates1978illustration} because it is able to overcome a common limitation of alternative multiple-comparisons statistical tests (e.g., the Friedman test \cite{friedman1937use}), namely the fact that such tests enable the possibility for one or more treatments to be classified in more than one group, thus making hard to the experimenter to really distinguish the real groups to which the means should belong \cite{carmer1985pairwise}. In particular, the test makes use of a clustering algorithm where, starting from the the whole group of observed mean effects, it divides, and keep dividing the sub-groups in such a way that the intersection of any two groups formed in that manner is empty. In other words, it is able to cluster the different ranks obtained into statistically distinct groups, making more sound and easier the interpretation of results. For these reasons, this test is highly recommended and particularly suitable in our context. With respect to the findings reported in the original version of our manuscript, we did not find any relevant difference. Thus, we could statistically confirm our observations also in the revised manuscript. We (i) better highlight that we actually did a statistical analysis of our results and (ii) re-run our statistical tests using the original version of the Scott-Knott test. The modifications are visible in Sections 4.3.5 and 4.4. We also further discussed the selection of these tests in Section 6.
}

\quotation{
- The authors need to discuss in more details the threats by describing the construct, internal and external validity threats. 
}

\answer{
We agree with this comment, especially after this careful review. We substantially improved Section 6 in order to carefully discuss the threats to the validity of our study. 
}



