\section*{Answers to Reviewer \#3}

\quotation{
The paper describes a qualitative and quantitative study of the relation between community smells and code smells. Overall, I feel that the topic and goals of this submission are of interest to the Software Engineering research and practitioner community. However, there are some issues with the current version of the manuscript. The main one being that the two parts of this submission are not well integrated and just at the surface happen to address the same topic (in fact, based on writing style they even read as if they are written by two different subsets of authors).
}	

\answer{We would like to thank the Reviewer for the provided feedback that were instrumental in improving our work. We tried to address all of the comments as detailed below.
	
\textcolor{blue}{To facilitate the new round of review, we have highlighted in blue in the manuscript the main changes/additions (i.e., we did not highlight minor fixes and writing issues).}}

\quotation{
For example, the qualitative study (a survey amongst 162 developers) identifies four community smells that influence the removal or non-removal of code smells. However, the quantitative study that aims to assess the impact of community smells on the intensity of code smells does not look at the four community smells identified in the qualitative study, it looks at a different set of four smells that shares only one smell with the initial study. This leads to a number of questions:\\
- why are these two studies combined in a single paper?\\
- why were the three ``new'' community smells used in the quantitative study not found in the qualitative study? Are they artificial? Are they not important to developers?
}

\answer{
The relation between first and second study has been one of the major issues encountered by all the Reviewers, and we thank them for pointing out this important problem of the previously submitted manuscript. In the former version of Section 2 we did not report (or we only rapidly mentioned) an important filtering we did when analyzing the survey data: specifically, in Figure 2 and in the related discussion we only reported the codes which developers referred to at least twice, i.e., we did not report the reasons to not refactor that were mentioned by at least two developers. We are sorry for not having reported the entire set of answers provided by them.

These answers are of a paramount importance because actually make the relation between the two studies even clearer than what explained when answering to the first comment of the Reviewer. In particular, the previously discarded answers provide evidence that the three community smells considered in the second study but not discussed in the first one, i.e., Organizational Silo Effect, Lone Wolf, and Bottleneck, might still represent causes behind a missing refactoring action aimed at removing code smells. 

For instance, one participant reported that the reason why she did not refactor a Long Method instance was because ``I'm the only working on it and I don't care of whether the others believe I should do it''. This answer was coded as ``Lone-Wolf'' in our dataset but, unfortunately, not reported because of the aforementioned filter. The same happened with the other two community smells. 

In the revised manuscript, we addressed this major issue as follows:

\begin{enumerate}
	\item We reported *all* the available survey data, improving the discussion of the results (Section 3.4) as well as the summary of findings reported in Section 3.5;
	
	\item We substantially improved Section 3.1, which reported the context of the second study, with the aim of making explicit that all the community smells investigated in the second part of the paper are related to the first one.
\end{enumerate}
}

\quotation{
- what is the influence of the missing three community smells identified in the survey on the intensity of code smells?
The paper never discusses any of these questions.
}

\answer{
This is really a good point. There are three main observations that need to be done to properly answer this comment.  

Firstly, it is important to remark that, as explained in Section 1, we followed a convergence mixed-method research approach, where the two studies were run in parallel, meaning that we the machine learning modeling was built and assessed at the same time of the survey. Thus, we were not aware of the new community smells at the time we performed the study and, therefore we could not assess their impact on the intensity of code smells.

Of course, we could re-run the machine learning study after having observed the results of the survey. However, this was not a viable solution because also in this case we could not evaluate what is the effect of the newly discovered community smells. The simple reason for that is represented by the lack of automated mechanisms to detect them: up to now, we have still not defined possible heuristics to detect the new community smells and, independently from this, the definition of an automated technique would require a specialized methodology aimed at devising the rules and validate them empirically. For these reasons, we preferred to avoid the analysis of these smells in the quantitative study.

Nevertheless, we believe that the paper is now much more cohesive than before, thank to the filter removal from the results of the survey study. In other words, we now present a paper where all the automatically detectable community smells emerged in the survey are considered in the quantitative study. 

In the revised manuscript, we better clarified the motivations why we do not consider the newly emerged community smells (see Section 6), and clarified that the definition of an automated technique for detecting them, as well as a replication of our study that takes into account them, is part of our future research agenda (see Section 8).
}

\quotation{
The paper (especially in the first qualitative part) uses the terminology ``maintaining/maintainability of code smells'' which is not standard and promotes misunderstanding. In the context of software engineering literature, maintainability is generally used to express the intent of actively promoting that a system/feature remains ``alive'' throughout a system's evolution. However, no developer will actively seek to keep a code smell around. I fully agree with the authors finding that they may not seek to remove a smell to avoid community problems, but that is not the same thing as actively promoting it stays around. It creates misunderstanding when the terms maintainability or maintaining are used for avoiding smell removal (in other places you describe this stasis well, so I don't think the paper needs the confusing ``maintaining/maintainability of code smells'' terminology). 
}

\answer{
Thanks, this is really a good suggestion. We avoided the use of the term ``maintainability'' in the revised version of the paper. At the same time, following the concerns of Reviewer 2, we have also included a new section that presents the terminology adopted in the paper (this is the new Section 2).
}

\quotation{
The choice of research methods is not well motivated, esp. the choice of using a classifier for analyzing factors that intensify code smells in the quantitative study. The authors should motivate in more detail why this choice is better in their situation than the more standard approach where principal component analysis is used to measure the contributions of different factors. 
}

\answer{
The revised manuscript now provides a more apt and fleshed-out motivation of the research design decisions and how they fit in the evaluation protocol for our convergence mixed-methods research design. Furthermore, we provide a theoretical convergence section which details how the two parts of the theory converge with a confirmatory connotation (this is the new Section 5).
}

\quotation{
Related to this point is the choice of statistical methods and their implementation. The authors apply the Scott-Knot Effect Size Difference set which is not part of the standard statistical literature or any of the main statistical packages but a home-brew statistical test developed and implemented by other software engineering researchers. While I believe the goals of the proposed test are respectable, I would prefer to see the conclusions being corroborated by proven tests from the standard statistical literature.
}

\answer{
The Reviewer is right, thanks for this suggestion. Following the recommendation, in the revised submission we replaced the Scott-Knott ESD test with two new tests that belong to the standard statistical literature. For this reason, we decided to apply the classical Scott-Knott test \cite{ScottKnott}. This test is widely used as a multiple comparison method in the context of analysis of variance \cite{gates1978illustration} because it is able to overcome a common limitation of alternative multiple-comparisons statistical tests (e.g., the Friedman test \cite{friedman1937use}), namely the fact that such tests enable the possibility for one or more treatments to be classified in more than one group, thus making hard to the experimenter to really distinguish the real groups to which the means should belong \cite{carmer1985pairwise}. In particular, the test makes use of a clustering algorithm where, starting from the the whole group of observed mean effects, it divides, and keep dividing the sub-groups in such a way that the intersection of any two groups formed in that manner is empty. In other words, it is able to cluster the different ranks obtained into statistically distinct groups, making more sound and easier the interpretation of results. For these reasons, this test is highly recommended and particularly suitable in our context. With respect to the findings reported in the original version of our manuscript, we did not find any relevant difference. Thus, we could statistically confirm our observations also in the revised manuscript. We (i) better highlight that we actually did a statistical analysis of our results and (ii) re-run our statistical tests using the original version of the Scott-Knott test. The modifications are visible in Sections 4.3.5 and 4.4. We also further discussed the selection of these tests in Section 6.
}

\quotation{
I would argue that the data presented in table 4 does not really support the conclusion on page 11 for RQ2 that ``The results are (i) consistent over all the individual systems in our dataset and (ii) statistically signiﬁcant, since the Scott-Knott ESD rank systematically reports ``Basic + Comm.-Smells + Comm.-Factors'' before ``Basic + Comm.Smells'' and ``Basic'' (see column ``SK-ESD'' in Table 4).''
The effect size differences in a good number of cases are very very small, and for the cases where the effect size difference to Basic is reasonable, this difference that seems to be more due to Community Factors than due to Community Smells. The same concern holds for the Summary for RQ2 a few lines later.
}

\answer{
We partially agree with this comment. On the one hand, it is true that the major increment in performance is due to community-related factors, which indicates that taking into account the development community is extremely relevant to understand technical-related tasks. On the other hand, the addition of community smell information provides benefits over all all the datasets exploited: this is visible when considering all the performance indicators. Sometimes the difference is relatively small, however we see this point as important as well: indeed, despite the presence of community-related factors, community smells still help improving the performance of the model. This means that there is always a gain in considering such smells. In any case, we agree with the Reviewer on the need to better clarify these aspects. In the revised paper, we toned down the claims present in the previous manuscript in order to better clarify and circumstantiate the role of community smells in the devised prediction model. 
}

\quotation{
Table 4 could use visual cues to guide the reader through the large amount of data that is presented. For example, consider using bold fonts of gray backgrounds for cells of interest, as you have done for other, less complex tables.
}

\answer{
Good point, thanks. We improved the visualization of the table.
}

\quotation{
For answering RQ3, the authors use a group of only five subjects for evaluating meaningfulness of the proposed code smell intensity prediction model. This is too small a group for an objective and representative evaluation, which undermines the credibility of the conclusions for RQ3. The authors briefly touch upon this topic in their threats to validity discussion, but the fact that these 5 people are experienced and have deep knowledge does not address the threat of not having a representative sample (it may even make matters worse because they will likely have stronger personal opinions).
}

\answer{
We are aware that having a set of 5 people does not allow the generalizability of their opinions. However, it is important to note that this part of the study was not designed to be quantitative or to provide insights that are as generalizable as possible. Rather, we aim at understanding better the meaning of the quantitative numbers coming from the devised prediction model: in other words, it should be seen as a really qualitative investigation conducted with experts that can provide us with interpretations of the results of the model. 

That being said, during the time of the major revision we did our best to recruit other participants, but - as the Reviewer will understand - this was certainly not an easy task because of personal and job-related commitments that project managers usually have. We were able to find additional 5 project managers willing to participate to our study: thus, in the revised paper the qualitative investigation involves 10 project managers having between 5 and 18 years of experience. At the same time, following the recommendations of Reviewer 2 we included more methodological details and discussion of the results achieved from this analysis (see Sections 4.4 and 4.5). 
}

\quotation{
In general, a more systematic treatment of the threats to validity is needed. I would suggest using the categories described in Yin's book on Case Study Research to avoid that factors are overlooked: construct validity (choice of methods), internal validity (e.g. choice of analysis and statistical tests and their implementation), external validity (generalizability, relating to the topic discussed above). For example, the authors currently mention the threat of using Deckard but omit the implementation of ESD or Decor.
}

\answer{
We totally agree on this point. Also in line with requirements from previous Reviewers, We substantially improved the threats to validity section in order to be more complete and precise on the potential factors influencing our findings and how we mitigated them. 
}

\quotation{
I find the relation of the first part of the title to the topic of the manuscript to be unclear. It certainly does not make sense in the context of the English expression ``There's plenty of other/more fish in the sea''. Some explanation is needed or a title revision should be considered.
}

\answer{
Up to now, to the best of our knowledge, the research on code smells mainly focused on the understanding of what are the technical aspects of source code that, e.g., can indicate the presence of design issues, can be perceived by developers, can amplify their effects, can be used to understand under which conditions developers perform refactoring, etc. Thus, as a matter of fact there is no study yet that investigates whether and what are the community-related factors influencing code smells. We captured this lack of studies and came up with this title, that aims at highlighting how there are other aspects to take into account in the enormous research field that is code smell detection and management, i.e., there are other fish in the large sea. For the mentioned reasons, we would like to keep the title as is; however, if the Reviewer really wants us to avoid this kind of metaphors, we will change the title.  
}

\quotation{
The language and writing of the manuscript could be greatly improved. Particularly the first part of the paper is formulated sloppily. Many sentences are far too long and contain too many sub-sentences that end up completely obscuring the point. As an example (page 6, above summary of RQ1): ``Therefore, in our concurrent quantitative study, we expected to observe a community-related increase of the intensity of code smells, in conjunction with code-clones (to account for potential code replication and smells spreading, as suggested by several developers), code-coupling (to account for decoupling consequences of refactoring) connected to community smells and other well-known community structure circumstances we observed in the survey, for example, truck-factor, and socio-technical congruence.''
}

\answer{
Once again, we totally agree with the Reviewer. We carefully went over the entire text and simplified it as much as possible. The new revision of the paper is certainly largely improved under this point of view. Thanks.
}

\quotation{
More detailed comments (the page numbers refer to manuscript page numbering, add 2 for the PDF page number):

- page 2: ``in parallel with qualitative inquiry,'' - needs a word between with and qualitative\\
- page 2: ``causes of'' $->$ ``causes for''\\
- page 2: ``our willingness to analyze'' $->$ ``our desire to analyze''\\
- page 5: de-coupling $->$ decoupling (twice)\\
- page 6, page 10: irregardless is not a word $->$ regardless \\
- consider replacing some of the 14 occurrences of the very Latin ``recurrent'' by the more widely understood ``repeated'', to make the text easier to read (especially recurrently $->$ repeatedly)\\
- The formulation of RQ3 could be improved, a community-aware code smell intensity prediction model does not ``improve the performance of models that do not consider this information'' it "performs better than models that do not consider this information" or it ``is an improvement over models that do not consider this information''. This occurs both on page 2 and page 5.\\
- page 9. line 20, right column: ``in the presence of the same code smell.'' $->$ I assume this should have been ``in the presence of the same *community* smell.''\\
- page 11, ``diagnosing the maintainability of code smells'' - please rephrase in more clear terms\\
- page 11: ``golden set'' $->$ ``ground truth''
}

\answer{
Thanks for these detailed comments. We fixed all of them. 
}


	