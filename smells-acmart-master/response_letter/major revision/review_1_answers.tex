% !TEX root = response_letter.tex

\section*{Answers to Reviewer \#1}

\quotation{SUMMARY\\
	In this paper, the authors conduct a survey with 162 developers to investigate whether code smells are affected by community smells. The results show that social factors can highly influence the decision of developers to refactor code smells. In a second study, the authors analyze 117 releases to measure the extent to which community smells impact code smell intensity. Although community smells influence code smells, we also see that technical factors have a stronger influence on code smells. Finally, the authors compare 3 different code smell intensity prediction models (basic, basic + community smells, and basic + community smells + community factors) and show that the latter models have an improved accuracy.
}

\answer{We would like to thank the Reviewer for the provided feedback that were instrumental in improving our work. We tried to address all of the comments as detailed below.

\textcolor{blue}{To facilitate the new round of review, we have highlighted in blue in the manuscript the main changes/additions (i.e., we did not highlight minor fixes and writing issues).}}

\quotation{GENERAL COMMENT\\
	I found the first part of the study (RQ1) extremely interesting and relevant. It's very important to know what are the social factors that hinder developers from refactoring. The survey is very well designed and executed and gives very interesting insights.
	Regarding the second part, the authors show that community smells and factors can predict the future severity of code smells with a higher accuracy. What is the practical applicability and usefulness of such a model, when the developers are already aware of the code smells (80\% of the developers know about the presence of the code smells), and more importantly they don't want to address them due to social reasons (80\% of the developers avoid refactoring due to community smells). It seems that community smells are much more difficult to overcome than technical (code) smells. As a result, a code smell affected by a community smell is not likely to be actionable. I think the motivation behind the prediction model is not well justified. The same holds for the implications and practical applications of the prediction model.
}

\answer{Thanks for sharing these potential actionability issues. While we can agree with most of the reported comments, we still believe that (1) a code-smell-intensity prediction model is needed and (2) it can be highly actionable in practice because it would help developers and project managers in better facing both technical- and community-related activities. 

More specifically, we see four main motivations for having a model like the one proposed in the paper:

\begin{enumerate} 
	
	\item As correctly said by the Reviewer, a code smell that co-occurs with a community smell may be not taken into account by a developer. While this can be true, the role of prediction models is indeed that of anticipating situations where the co-occurrence might lead to more serious maintainability issues. For example, suppose that (a) in a certain release R$_i$, a code smell has a ``LOW'' intensity and that (b) our prediction model predicts the intensity of this instance to be ``HIGH'' --- the features of the model related to community smells mark the subsequent release R$_{i+1}$ as more prone to a decrease of the overall maintainability of the affected classes. In this case, a project manager may immediately take action, trying to fix the community-related issues with the aim of preventing the smell to increase in its intensity. This is the key use case scenario of our work and we honestly believe on its practical usefulness and actionability.

	\item As shown by recent papers \cite{PalombaBPOL14,taibi2017developers}, the perception of code smells heavily depends on their intensity. We argue that approaches able to precisely indicate the future severity of code smell instances might allow developers when (i) understanding the possible criticisms of the software program being developed that may arise in a short-term future and (ii) dealing with or simply monitoring their evolution \cite{palomba2017scent}.
	
	\item In the past, we have shown that the intensity of code smells has a strong impact on fault-proneness \cite{palomba2017diffuseness} and can be actually used to identify parts of source code that are likely to be defective \cite{palomba2017toward}: thus, intensity prediction models can help developers assessing when a certain refactoring or other program transformations must be applied to not incur in possible additional maintainability and/or reliability problems.
	
	\item Intensity can be used as a means for selecting the code smell instances that need to be more urgently fixed. As not all the smells are removed, an approach able to rank them based on their severity might be worthwhile to allow developers selecting the instances on which to focus more, or even those that are more relevant to manage because of the co-occurrence of a community smell.
	
\end{enumerate}	
	
For all these reasons, we strongly believe that the proposed prediction model can be immediately actionable in practice. At the same time, these reasons represent the main glue between the two studies of our paper: indeed, on the one hand we raise the need for taking into account community-related factors while assessing source code maintainability; on the other hand, we also show how this is possible, i.e., how to exploit community-related information to devise a prediction model that can help practitioners in preventively fix possible issues during software maintenance and evolution. 

In the revised version of the paper, we added a new recap in Section 4, where we reported rationale and motivations why the definition of a code-smell-intensity prediction model makes sense. 
}

\quotation{COMMENTS
1) Survey design for RQ1\\
1a) ``Thus, we contacted 472 developers that worked with one distinct smelly class instance in any of the releases we considered.''\\
Does this mean you excluded the developers that worked with more than one smelly class instance?\\
How many developers are in this category?\\
For this category of developers, you could have randomly selected one of the smelly class instances to contact them.
}

\answer{Yes, we excluded developers that worked with more smelly classes. The rationale here is that developers who worked on several smelly classes might potentially not really be focused on the history of one specific smelly class, e.g., they might have confused situations appearing in the context of another smelly class with those of the class we were targeting. To avoid any possible bias, we preferred to be conservative and exclude them from the target population of our survey: all in all, we discarded 168 developers. In the revised paper, we clarified both the motivation and final number of developers discarded from this analysis (see Section 3.3).}

\quotation{
1b) ``As a result, 162 developers responded out of the 472 contacted ones, for a response rate of 34,32\%''\\
The fact that you targeted developers involved in smelly class instances, definitely contributed to this high response rate. Is there any other study design choice that you did, which might have contributed positively to the achieved response rate?\\
For example, are the developers who replied still active developers in the projects? Are the developers who replied among the most active developers in the projects?\\
Such information would be really helpful for other researchers who want to design a similar study and can help them to target specific profiles of developers who are more likely to respond.
}

\answer{We agree with the Reviewer, the addition of this information would be useful for other researchers: we added the following discussion in Section 3.3. 

While it can be possible that the fact of targeting only developers working on code smell instances contributed to the high response rate, in our opinion there are three aspects that have represented the key to achieve it:

\begin{enumerate} 
	
	\item We contacted developers that committed the highest number of changes to a smelly class and using personalised e-mails (as per guidelines by Singer and Vinson \cite{SingerV02}): this means that we only targeted developers who were expert of the considered classes; for example, they might have been those more interested in gathering further information on the class they were mainly in charge of.
	
	\item Looking at the overall number of commits, we can also confirm that the involved developers are among the most active in the project; at the same time, 95\% of them (i.e., 154/162) still committed changes in the time given for this major revision, meaning that they continue to be extremely active now;
	
	\item The time and amount of information required to developers were limited: as explained in the paper, we opted for a cognitively simple set of questions to encourage them to reply to our e-mails, still following the guidelines from Singer and Vinson \cite{SingerV02}.
	
\end{enumerate} 	
}

\quotation{
1c) I downloaded the dataset from the online appendix [78], but I could not find anything related to RQ1. There is a technical report about the detection of 4 community smells, and a spreadsheet with all the metrics extracted from the repositories for RQ2.
}

\answer{That was our fault, sorry. We uploaded the developers’ answer in a spreadsheet file in our online appendix, which is directly accessible here (https://tinyurl.com/RawRQ1data) for peer-review only.}

\quotation{2) Section 3.1\\
Out of all the community smells reported by the developers in RQ1, only Black-cloud is included in the second study. The Prima-donna smell, which was also reported by the developers, was excluded, because there is no detection approach. However, in Section 4.3 of the technical report/thesis [61], as well as in the online appendix [78], there is a detection approach for Prima-donna!!
The new community smells that emerged from your study were also excluded. The Organizational Silo, Lone-wolf, and Bottleneck community smells included in your study, were not reported by a single developer.
This actually weakens the link between the two studies, because only one community code smell is common between them.
}

\answer{
The relation between first and second study has been one of the major issues encountered by all the Reviewers, and we thank them for pointing out this important problem of the previously submitted manuscript. In the former version of Section 2 we did not report (or we only rapidly mentioned) an important filtering we did when analyzing the survey data: specifically, in Figure 2 and in the related discussion we only reported the codes which developers referred to at least twice, i.e., we did not report the reasons to not refactor that were mentioned by at least two developers. We are sorry for not having reported the entire set of answers provided by them.

These answers are of a paramount importance because actually make the relation between the two studies even clearer than what explained when answering to the first comment of the Reviewer. In particular, the previously discarded answers provide evidence that the three community smells considered in the second study but not discussed in the first one, i.e., Organizational Silo Effect, Lone Wolf, and Bottleneck, might still represent causes behind a missing refactoring action aimed at removing code smells. 

For instance, one participant reported that the reason why she did not refactor a Long Method instance was because ``I'm the only working on it and I don't care of whether the others believe I should do it''. This answer was coded as ``Lone-Wolf'' in our dataset but, unfortunately, not reported because of the aforementioned filter. The same happened with the other two community smells. 

In the revised manuscript, we addressed this major issue as follows:

\begin{enumerate}

	\item We reported *all* the available survey data, improving the discussion of the results (Section 3.4) as well as the summary of findings reported in Section 3.5;
	
	\item We substantially improved Section 3.1, which reported the context of the second study, with the aim of making explicit that all the community smells investigated in the second part of the paper are related to the first one.
	
\end{enumerate}

For the sake of completeness, let us comment on the Prima Donna identification mechanism. Also in this case, we did not clarify well the reasons why we needed to exclude it. While the document in appendix actually reports a detection mechanism for this smell, it does not report hints on its validation. Experimenting with the devised operationalisation revealed a considerable quantity of false positives (around 62\%); consequently we discovered that the methodology we initially defined was not good at all to spot this type of community smell. At the same time, we believe that a better operationalization requires further understanding of the socio-technical and structural conditions which reflect the prima donna effect - in that respect, we are engaged in several studies with alternative mechanisms (e.g., sentiment analysis). 
}

\quotation{I am very curious about why you focused on these specific community smells. I am not an expert in community smells, but I went through some references cited in the paper and discovered there is a big number of community smells. In [93], there are 9 community smells, only three of them are included in this study. In [91], there are 12 community smells, none of them is included in this study.
If there are no detection approaches in the literature, why don't you propose some approach in this work? Is it impossible to detect these community smells with the information available in repositories?}

\answer{
The perplexity of the Reviewer is perfectly understandable. Indeed, the detection mechanism for the smells in question is currently undergoing major revision in the IEEE Transactions on Software Engineering and should hopefully appearing sometime later this year. On the paper in question, we illustrate the \textsc{CodeFace4Smells} tool we built and evaluated to prepare the operationalisation that the Reviewer is currently addressing. The tool itself, however, is limited to detecting the same smells which are involved in this study (see Sections 2 and 3.1) - in the future work of that paper we are highlighting the next steps we are considering to advance further into specifying and refining the tool to include an operationalisation of more smells, e.g., more complex smells that demand the triangulated use of additional techniques (e.g., as previously remarked, the use of sentiment analysis, which one of the co-authors is well versed in). In the scope of this study we chose to focus on the most recurrent smells from \cite{TamburriKLV15} which can be operationalised with structural and social-networks analysis metrics.
}

\quotation{
3) Section 3.2\\
3a) What kind of communication do the edges of the communication graph represent? How do you define communication?\\
3b) Black cloud and Bottleneck smells require the application of a clustering algorithm. Which clustering algorithm should I use, and how should I configure it? Some clustering algorithms take as input the number of clusters, others take as input a cutoff value.\\
3c) The formal definition of the community smells is not readable at all, especially for Black-cloud and Bottleneck. Please organize the formal definitions better. Use a separate line for each sub-expression and add comments to explain each line.
}

\answer{
3.a Our operationalisations inherit all terms, definitions, and approaches evaluated and successfully published at the meeting of the 37th International Conference on Software Engineering (ICSE) in 2015 by Joblin et al. \cite{JoblinMASR15} - we therefore encourage the Reviewer to inspect said terms and definitions from Joblin's seminal work on the topic of Verified Software Community Analysis, which is the technical basis of our own tool \textsc{CodeFace4Smells}, an augmented fork of Joblin's original tool called \textsc{CodeFace}. More specifically, In Joblin et al.'s function-based approach, they use a fine-grained heuristic based on code structure blocks and code-function communication, where developers are considered to be coordinated and communicating when they contribute code to a common function block upon which they co-comment inline, or exchange mails reflecting such function blocks.
	
\noindent 3.b The black cloud and bottleneck smells harness the use of community detection based on the ``Order Statistics Local Optimization Method'' (OSLOM) \cite{oslom} featured inside \textsc{CodeFace}, which was never previously done before on developer networks. OSLOM is one of the few methods that is able to handle weighted and directed networks and to form overlapping community clusters --- to clarify this point, we elaborated and appropriately cited the use of the reference algorithm.
	
\noindent 3.c - thanks for the comment, the suggestion was applied in the main body of the manuscript.
}

\quotation{
GRAMMATICAL ISSUES/TYPOS\\
Page 2: ``community smells are top community-related factors perceived.''\\
Something is wrong with the grammar in this sentence.
	
Page 10: ``an overall F-Measure 2\%, 3\%, 4\%, 3\%, and 13\% higher that''\\
``that'' $->$ ``than''
}

\answer{Thanks, we fixed them.}
