\documentclass[10pt,journal,compsoc]{IEEEtran}

\usepackage{booktabs} % For formal tables

\usepackage{balance} 

{\newcommand{\nb}[2]{
	\fbox{\bfseries\sffamily\scriptsize#1}
	{\sf\small$\blacktriangleright$\textit{#2}$\blacktriangleleft$}
}
\newcommand\DAMIAN[1]{\nb{Damian}{#1}}
\newcommand\FABIO[1]{\nb{Fabio}{#1}}
\newcommand{\as}[1]{\nb{Alexander}{#1}}
\newcommand\ANDY[1]{\nb{Andy}{#1}}
\newcommand\ROCCO[1]{\nb{Rocco}{#1}}

\newcommand\tvert[1]{\parbox[t]{2mm}{\rotatebox[origin=c]{90}{#1}}}

\newcommand{\ie}{\textit{i.e.,}\space}
\newcommand{\eg}{\textit{e.g.,}\space}
\newcommand{\etc}{\textit{etc.}\space}
\newcommand{\etal}{\textit{et al.}\space}

\newcommand{\CodeFace}{\textsc{CodeFace}}
% Tables settings
\usepackage{booktabs}

\usepackage{float}
\usepackage{rotfloat}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{multirow}

\makeatletter

\let\pr@chap=\pr@cha
\pdfpageheight\paperheight
\pdfpagewidth\paperwidth

\floatstyle{ruled}
\providecommand{\algorithmname}{Algorithm}
\floatname{algorithm}{\protect\algorithmname}

\usepackage{algpseudocode}% http://ctan.org/pkg/algorithmicx

\usepackage{listings}
\renewcommand{\lstlistingname}{Listing}

\usepackage[ruled]{algorithm2e}
\renewcommand{\algorithmcfname}{ALGORITHM}
\usepackage{array} 
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{lineno}
\usepackage{framed}
\usepackage{float}
\restylefloat{table}
\usepackage{rotating}
\usepackage{subfigure}
\usepackage{enumitem}
\usepackage{color}
\usepackage{framed}
\usepackage{colortbl}
\usepackage{amsmath}
\usepackage{subfigure}
\usepackage{url}
\usepackage{xcolor}
\usepackage{bbding}
\usepackage{url}

\definecolor{gray50}{gray}{.5}
\definecolor{gray40}{gray}{.6}
\definecolor{gray30}{gray}{.7}
\definecolor{gray20}{gray}{.8}
\definecolor{gray10}{gray}{.9}
\definecolor{gray05}{gray}{.95}

\newlength\Linewidth
\def\findlength{\setlength\Linewidth\linewidth
	\addtolength\Linewidth{-4\fboxrule}
	\addtolength\Linewidth{-3\fboxsep}
}
\newenvironment{examplebox}{\par\begingroup
	\setlength{\fboxsep}{5pt}\findlength
	\setbox0=\vbox\bgroup\noindent
	\hsize=0.95\linewidth
	\begin{minipage}{0.95\linewidth}\normalsize}
	{\end{minipage}\egroup
	%    \vspace{6pt}
	\textcolor{gray20}{\fboxsep1.5pt\fbox
		{\fboxsep5pt\colorbox{gray05}{\normalcolor\box0}}}
	%    \endgroup\par\addvspace{6pt minus 3pt}\noindent
	\endgroup\par\noindent
	\normalcolor\ignorespacesafterend}
\let\Examplebox\examplebox
\let\endExamplebox\endexamplebox

\newenvironment{conjecturebox}{\par\begingroup
	\setlength{\fboxsep}{5pt}\findlength
	\setbox0=\vbox\bgroup\noindent
	\hsize=0.95\linewidth
	\begin{minipage}{0.95\linewidth}\normalsize}
	{\end{minipage}\egroup
	%    \vspace{6pt}
	\textcolor{gray20}{\fboxsep1.5pt\fbox
		{\fboxsep5pt\colorbox{white}{\normalcolor\box0}}}
	%    \endgroup\par\addvspace{6pt minus 3pt}\noindent
	\endgroup\par\noindent
	\normalcolor\ignorespacesafterend}
\let\Examplebox\examplebox
\let\endExamplebox\endexamplebox

\hyphenation{op-tical net-works semi-conduc-tor}
\newcommand\revised[1]{\textcolor{black}{#1}}
\newcommand\minor[1]{\textcolor{black}{#1}}

\begin{document}
	
	\title{\minor{Beyond Technical Aspects: How Do Community Smells Influence the Intensity of Code Smells?}}

	\author{Fabio Palomba,~\IEEEmembership{Member,~IEEE,}
				  Damian A.~Tamburri,~\IEEEmembership{Member,~IEEE,}\\
				  Francesca Arcelli Fontana,~\IEEEmembership{Member,~IEEE,}
				  Rocco Oliveto,~\IEEEmembership{Member,~IEEE,}\\
				  Andy Zaidman,~\IEEEmembership{Member,~IEEE,}
				 Alexander Serebrenik,~\IEEEmembership{Senior Member,~IEEE.}

\IEEEcompsocitemizethanks{
	
	\IEEEcompsocthanksitem F. Palomba is with the University of Zurich, Switzerland;
	E-mail: palomba@ifi.uzh.ch
	\IEEEcompsocthanksitem D. A. Tamburri and A. Serebrenik are with Eindhoven University of Technology, The Netherlands;
	E-mail: d.a.tamburri, a.serebrenik@tue.nl
	\IEEEcompsocthanksitem A. Zaidman is with Delft Unversity of Technology, The Netherlands; 
	E-mail: a.e.zaidman@tudelft.nl
	\IEEEcompsocthanksitem F. Arcelli is with University of Milano Bicocca, Italy; 
	E-mail: arcelli@unimib.it
	\IEEEcompsocthanksitem R. Oliveto is with University of Molise, Italy; 
	E-mail: rocco.oliveto@unimol.it}
}

\markboth{Transactions on Software Engineering,~Vol.~XX, No.~XX, XYZ~XXXX}%
{Palomba \MakeLowercase{\textit{et al.}}: }

\IEEEtitleabstractindextext{%
\begin{abstract}
\revised{Code smells are poor implementation choices applied by developers during software evolution that often lead to critical flaws or failure. Much in the same way, community smells reflect the presence of organizational and socio-technical issues within a software community that may lead to additional project costs. Recent empirical studies provide evidence that community smells are often---if not always---connected to circumstances such as code smells. In this paper we look deeper into this connection by conducting a mixed-methods empirical study of 117 releases from 9 open-source systems. The qualitative and quantitative sides of our mixed-methods study were run in parallel and assume a mutually-confirmative connotation. On the one hand, we survey 162 developers of the 9 considered systems to investigate whether developers perceive relationship between community smells and the code smells found in those projects. On the other hand, we perform a fine-grained analysis into the 117 releases of our dataset to measure the extent to which community smells impact code smell intensity (i.e., criticality). We then propose a code smell intensity prediction model that relies on both technical and community-related aspects. The results of both sides of our mixed-methods study lead to one conclusion: community-related factors contribute to the intensity of code smells. This conclusion supports the joint use of community and code smells detection as a mechanism for the joint management of technical and social problems around software development communities.}
\end{abstract}


\begin{IEEEkeywords}
Code smells, organizational structure, community smells, mixed-methods study
\end{IEEEkeywords}}
 
\maketitle

\IEEEdisplaynontitleabstractindextext
\IEEEpeerreviewmaketitle

\section{Introduction}\label{sec:intro}
\input{intro}

\section{Terminology}\label{sec:terminology}
\input{terminology}

\section{Surveying Software Developers}\label{sec:survey}
\input{survey}

\section{Community vs. Code Smells}\label{sec:rd}
\input{resdes}

\section{\revised{Theoretical Convergence}}\label{converge}
\input{convergence}

%\input{model-results}
\section{Threats to Validity}\label{sec:threats}
\input{threats}

\section{Related Work}\label{sec:related}
\input{related}

\section{Conclusion}\label{sec:conc}
\input{conc}

\section*{Acknowledgment}
The authors would like to thank the Associate Editor and anonymous Reviewers for the insightful comments that allowed to significantly strengthen this paper. Palomba gratefully acknowledge the support of the Swiss National Science Foundation through the SNF Project No. PP00P2\_170529. 

\bibliographystyle{ieeetran}
\bibliography{smells} 
%\balance

\begin{IEEEbiography}[{\includegraphics[width=80pt,height=80pt]{bio/palomba.jpg}}]{Fabio Palomba}
	is a Senior Research Associate at the University of Zurich, Switzerland. He received the PhD degree in Management \& Information Technology from the University of Salerno, Italy, in 2017. His research interests include software maintenance and evolution, empirical software engineering, source code quality, and mining software repositories.
	He was also the recipient of two ACM/SIGSOFT and one IEEE/TCSE Distinguished Paper Awards at ASE'13, ICSE'15, and ICSME'17, respectively, and a Best Paper Awards at CSCW'18 and SANER'18. He serves and has served as a program committee member of various international conferences (e.g., MSR, ICPC, ICSME), and as referee for various international journals (e.g., TSE, TOSEM, JSS) in the fields of software engineering. Since 2016 he is Review Board Member of EMSE. He was the recipient of four Distinguished Reviewer Awards for his reviewing activities conducted for EMSE, IST, and JSS between 2015 and 2018. 
	%He co-organized the 2nd International Workshop on Machine Learning Techniques for Software Quality Evaluation (MaLTeSQuE 2018). He is also Guest Editor of special issues related to his research interests that will appear in EMSE and JSEP.
\end{IEEEbiography}
\
\begin{IEEEbiography}[{\includegraphics[width=72pt,height=90pt]{bio/tamburri.png}}]{Damian Andrew Tamburri}
	is an Assistant Professor at the Jheronimus Academy of Data Science (JADS) and the Technical University of Eindhoven (TU/e). His research interests include social software engineering, advanced software architectures, design, and analysis tools as well as advanced software-engineering methods and analysis tools. He is on the IEEE Software editorial board and is secretary of the International Federation for Information Processing Working Group on Service-Oriented Computing. Contact him at d.a.tamburri@tue.nl or dtamburri@acm.org.
\end{IEEEbiography}

\begin{IEEEbiography}[{\includegraphics[width=72pt,height=90pt]{bio/arcelli.png}}]{Francesca Arcelli Fontana} 
	has her degree and Ph.D in Computer Science taken at the University of Milano (Italy). She is currently in the position of Associate Professor at University of Milano Bicocca. The actual research activity principally concerns the software engineering field. In particular in software evolution, software quality assessment, model-drive reverse engineering, managing technical debt, design patterns and architectural smells detection. She is the head of the Software Evolution and Reverse Engineering Lab (http://essere.disco.unimib.it/) at University of Milano Bicocca and she is a Member of the IEEE Computer Society.
\end{IEEEbiography}

\begin{IEEEbiography}[{\includegraphics[width=72pt,height=90pt]{bio/oliveto.png}}]{Rocco Oliveto} 
	is Associate Professor at University of Molise (Italy), where he is also the Chair of the Computer Science Bachelor and Master programs and the Director of the Software and Knowledge Engineering Lab (STAKE Lab).
	He co-authored over 100 papers on topics related to software traceability, software maintenance and evolution, search-based software engineering, and empirical software engineering. 
	His activities span various international software engineering research communities. He has served as organizing and program committee member of several international conferences in the field of software engineering. He was program co-chair of ICPC 2015, TEFSE 2015 and 2009, SCAM 2014, WCRE 2013 and 2012. He was also keynote speaker at MUD 2012.
	He is also one of the co-founders and CEO of datasounds, a spin-off of the University of Molise aiming at efficiently exploiting the priceless heritage that can be extracted from big data analysis. 
\end{IEEEbiography}

\begin{IEEEbiography}[{\includegraphics[width=72pt,height=90pt]{bio/zaidman.jpg}}]{Andy Zaidman} 
	is an associate professor at the Delft University of Technology, The Netherlands. He obtained his
	M.Sc. (2002) and Ph.D. (2006) in Computer Science from the University of Antwerp, Belgium. His main
	research interests are software evolution, program comprehension, mining software repositories and
	software testing. He is an active member of the research community and involved in the organization
	of numerous conferences (WCRE'08, WCRE'09, VISSOFT'14 and MSR'18). In 2013 Andy Zaidman was the
	laureate of a prestigious Vidi career grant from the Dutch science foundation NWO.
\end{IEEEbiography}

\begin{IEEEbiography}[{\includegraphics[width=72pt,height=90pt]{bio/serebrenik.jpg}}]{Alexander Serebrenik} 
	is an associate professor of Model-Driven Software Engineering at Eindhoven University of Technology. He has obtained his Ph.D. in Computer Science from Katholieke Universiteit Leuven, Belgium (2003) and M.Sc. in Computer Science from the Hebrew University, Jerusalem, Israel. Prof. Serebrenik's areas of expertise include software evolution, maintainability and reverse engineering, program analysis and transformation, process modeling and verification. He is involved in organisation of such conferences as ICSME, MSR, ICPC and SANER.
\end{IEEEbiography}

\end{document}
