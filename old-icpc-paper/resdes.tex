
	The \emph{goal} of the empirical study is to investigate the relationships between \emph{social} and \emph{technical} aspects of software development, with the \emph{purpose} of understanding to what extent community-related factors have an impact in the introduction of design defects. 
	
	In our research plan, the first {\bf RQ} is a preliminary step and aims at analyzing whether there exists an actual relationship between the two types of smells in the social/organizational and architectural/technical structure to which they refer.
	{\bf RQ$_1$}  is, instead, a \emph{fine-grained} investigation into the specific types of different code smells having a stronger relationship: indeed, in this {\bf RQ} we plan to analyzed which are the smell pair more likely to co-occur in a given software system.
	Finally, the goal of {\bf RQ$_2$} is to shed light on possible causal relationships between community and code smells. In particular, we initially investigate whether there exists a statistically significant trend in the way community smells lead to code smells. 

%a natural consequence of RQ$_0$, where we plan to investigate any co-occurrence of community and technical smells together. Much in the same way, understanding which smell types more frequently co-occur together leads to making efforts and predicting the ``temporal'' side of this co-occurrence, that is, RQ$_2$. First, we plan to compute the percentage of times a given community/technical smell X in a class C lead to the introduction of a technical/community smell Y in a future release of C. Then, we plan to confirm such a temporal relationship using (i) the Granger causality statistical test, and (ii) qualitative examples mined from the versioning system.
%Finally, concerning RQ$_3$, we plan to analyze whether the removal of a given community smell in a release R will lead to the removal of a technical smell in R+1. Essentially, with RQ$_2$ and RQ$_3$ we investigate how different smell types are born and die. Along this path of inquiry, we plan to articulate further into analyzing the *actual effect* of more smell types on maintainability and software evolution. An additional tentative RQ can be phrased as follows: ``Are classes where community and technical smells co-occur more prone to maintainability issues?'' As proxy measures for maintainability, we might use the change- and fault-proneness of the classes involved in more than one smell type, comparing them with (i) clean classes, (ii) other classes involved in technical smells only. Around this latter research question, our design would need to take into careful account possible co-factors such as size and code complexity, size, language and more - a preliminary qualitative study of these factors is currently in progress.

\subsection{Context of Study}
The \emph{context} of the study is represented by (i) subject systems, and (ii) community and code smells analyzed.
As for the former, we focused the attention on 117 releases of 9 open-source projects belonging to two large software ecosystems, \ie \textsc{Apache} and \textsc{Eclipse}. 
In particular, from the \textsc{Apache Foundation} we randomly selected 7 projects from the complete list of projects available on \textsc{GitHub}\footnote{https://github.com/apache}: these are \textsc{Mahout}, \textsc{Cayenne}, \textsc{Cassandra}, \textsc{Pig}, \textsc{Jackrabbit}, \textsc{Jena}, \textsc{Lucene}, and \textsc{Mallet}. From the \textsc{Eclipse} ecosystem we focused on \textsc{CDT} and \textsc{CFX}.
Our analysis refers to the \emph{major releases} issued between September 2015 and September 2016.

As for the smells analyzed, we considered 4 of the community smells defined by Tamburri \etal \cite{TamburriKLV15}, namely:

	\begin{enumerate}
		\item \emph{the Organisational Silo Effect:} siloed areas of the developer community that essentially do not communicate, except through one or two of their respective members;
		\item \emph{the Black-cloud Effect:} information overload connected to lack of structured communication or cooperation/collaboration governance practices;
		\item \emph{the Lone-wolf Effect:} unsanctioned or defiant contributors who carry out their work irrespective or irregardless of their peers (i.e., co-committing developers), their decisions and communication;
		\item \emph{the Bottleneck or ``Radio-silence" Effect:} an instance of the problem of ``unique boundary spanner" \cite{wasserman1994social} problem from social-networks analysis: one member interposes him/herself into every formal interaction across two or more sub-communities with little or no flexibility to introduce other parallel channels;
	\end{enumerate}

Such community smells have been studied in combination with the 5 technical smells listed in the following:

	\begin{enumerate}
		\item \emph{the Blob class:} a class usually characterized by a high number of lines of code, low cohesion, and that monopolizes most of the systems's processing \cite{Fowler:1999};
		\item \emph{the Class Data Should be Private:} a class that exposes its internal attributes, leading to the violation of the information hiding principle \cite{Fowler:1999};
		\item \emph{the Complex Class:} a class usually characterized by a very high cyclomatic complexity which make it poorly testable \cite{Brown:1998};
		\item \emph{the Functional Decomposition:} a class where the Object Oriented Programming (OOP) principles are poorly used (\eg inheritance and polymorphism not used) \cite{Fowler:1999};
		\item \emph{the Spaghetti Code:} a class without a well-defined structure, usually declaring many long methods \cite{Fowler:1999};
	\end{enumerate}
	
The choice of focusing on these smells has been driven by the willingness to understand how community-related issues impact the presence of different types of design defects (\eg highly complex classes or source code violating OOP principles).

\subsection{Data Extraction and Analysis}
	Once we had cloned the source code of the subject systems from the corresponding \textsc{Github} repositories (using the \texttt{git clone} command), we extracted the major releases issued in the time period taken into account considering the release history publicly available on the repositories analyzed, downloading them using the \texttt{git checkout} command.
	At the same time, we developed a tool for mining the developers' discussions stored in the issue trackers of analyzed projects.

	Afterwards, we ran two different detection techniques over the issue tracker (for community smells) and the source code (for code smells) of the subject systems. Specifically, we detected the considered smells using:

	\begin{itemize}
	%defined in previous and related work \cite{archcomm,TamburriN15}
		\item our own fork of the \textsc{CodeFace} tool \cite{JoblinMASR15}. The tool builds a developer network using committer-author relationships from a versioning system. Then, social network algorithms are used to detect the four community smells considered. For sake of space, detailed detection rules are available online \cite{appendix}. Note that we identified the community smells occurring in the release $R_i$ by considering the information on committer-author relationships accumulated over releases $R_{i-3}$ and $R_i$, where ``i=1" reflects 1 month worth of commit data. Our analysis therefore spans 3 months of community activity, as seen in previous research \cite{AmritSASNA}. 
		
		\item our re-implementation of the \textsc{DECOR} approach \cite{MohaG07}, which uses the exact rules proposed by Moha \etal to identify the code smells considered in the study\footnote{The detection rules are available at \url{http://www.ptidej.net}.}. 
	\end{itemize} 
	
After having extracted the instances of community and code smells affecting each of the releases analyzed, we answered {\bf RQ$_0$} by means of correlation analysis between the two smell types. 
Specifically, we computed the Spearman rank correlation coefficient \cite{Cohen:1988} between the instances of community and code smells present in each release considered. 
It is worth remembering that this is a preliminary investigation aimed at understanding whether the two different smell types actually present some kind of relationship. 

As for {\bf RQ$_1$}, we mined association rules \cite{Agrawal:1993} for detecting sets of community and code smells that often co-occur. 
In particular, this is an unsupervised learning technique able to discover hidden relationships in large datasets. 
In the context of this work, we applied the \textsc{aPriori} algorithm \cite{Agrawal:1993} on the dataset containing all the community and code smells retrieved in the considered releases. 
In Section \ref{sec:res} we report and discuss the association rules---involving smell pairs---having the higher strength in terms of support and confidence \cite{Agrawal:1993}.
Furthermore, we provided practical explanations behind the relationships identified. 

Finally, we answered {\bf RQ$_2$} by computing the Mann-Kendall statistical test \cite{Cohen:1988}. This is a non-parametric statistical test commonly employed to detect monotonic trends in series of data. 
In our context, we wanted to analyze whether there is a trend in the number of community and code smells affecting the considered releases over time.
In Section \ref{sec:res} we discuss the results achieved when applying the test on the smell pairs detected as highly related in the context of {\bf RQ$_1$}.


